# Sshade
Python program aiming at creating xml files for filling the spectroscopy database SSHADE.
The program takes a specific ods file and, based on several xml templates, creates the necessary xml files.

## To use this tool in a jupyter notebook:
You should have installed: lxml, pyexcel, pyexcel-ods
In the NoteBook, you should type:
import sys
sys.path.append("/mntdirect/_users/letard/Jupyter/Scripts/Sshade")
import csv2xml
my_directory = "" with the path of your data
TemplatePath = "" with the path of the xml template files

## Implemented functions:
- convertion of the ods file into as many csv files as spreadsheets in the ods file:
	use csv2xml.ods_converter(my_directory,"my_odsfile")

- for experimentalists, use csv2xml.experimentalist_file_writer(TemplatePath,my_directory,"my_csvfile")

- for laboratories, use csv2xml.laboratory_file_writer(TemplatePath,my_directory,"my_csvfile")

- for samples:
	-- for pellets, use csv2xml.sample_pellet_file_writer(TemplatePath,my_directory,"my_csvfile")
	-- for frozen solution, use csv2xml.sample_frozen_solution_file_writer(TemplatePath,my_directory,"my_csvfile")
	-- for HP/HT solution, use csv2xml.sample_HPHT_solution_file_writer(TemplatePath,my_directory,"my_csvfile")

- for experiments_spectra:
	-- for experiments with at max 1 varying parameter (instrument, spectral range -ie the edge-, sample composition or condition),
		use csv2xml.experiments_file_writer(TemplatePath,my_directory,"my_csvfile")
	-- for experiments with 2 varying parameters (instrument, sample composition or condition),
		use csv2xml.experiments_structure_file_writer(TemplatePath,my_directory,"my_csvfile")

- to process several files of the same type, use multiple_file_writer(TemplatePath,my_directory,"writer_type"),
	writer_type being experimentalist, laboratory, pellet, frozen_solution, HPHT_solution, experiments_spectra or
	experiments_spectra_with_structure.
