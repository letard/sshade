'''
TO USE THIS FILE IN A JUPYTER NOTEBOOK:
=======================================
You should have installed: lxml, pyexcel, pyexcel-ods
In the NoteBook, you should type:
import sys
sys.path.append("/mntdirect/_users/letard/Jupyter/Scripts/Sshade")
import csv2xml
my_directory = "" with the path of your data
TemplatePath = "" with the path of the xml template files

Several functions are implemented:
- convertion of the ods file into as many csv files as spreadsheets in the ods file:
	use csv2xml.ods_converter(my_directory,"my_odsfile")

- for experimentalists, use csv2xml.experimentalist_file_writer(TemplatePath,my_directory,"my_csvfile")

- for laboratories, use csv2xml.laboratory_file_writer(TemplatePath,my_directory,"my_csvfile")

- for samples:
	-- for pellets, use csv2xml.sample_pellet_file_writer(TemplatePath,my_directory,"my_csvfile")
	-- for frozen solution, use csv2xml.sample_frozen_solution_file_writer(TemplatePath,my_directory,"my_csvfile")
	-- for HP/HT solution, use csv2xml.sample_HPHT_solution_file_writer(TemplatePath,my_directory,"my_csvfile")

- for experiments_spectra:
	-- for experiments with at max 1 varying parameter (instrument, spectral range -ie the edge-, sample composition or condition),
		use csv2xml.experiments_file_writer(TemplatePath,my_directory,"my_csvfile")
	-- for experiments with 2 varying parameters (instrument, sample composition or condition),
		use csv2xml.experiments_structure_file_writer(TemplatePath,my_directory,"my_csvfile")

- to process several files of the same type, use multiple_file_writer(TemplatePath,my_directory,"writer_type"),
	writer_type being experimentalist, laboratory, pellet, frozen_solution, HPHT_solution, experiments_spectra or
	experiments_spectra_with_structure.

- simulated spectra can be handled in experiments with and without structure.

- some mandatory fields are checked:
	-- publications for experiments
	-- experimentalist for pellets, frozen solution, HPHT solutions and experiments
	-- diluent and material masses for pellets
	-- specie quantity for frozen solutions and HPHT solutions
	-- sample volume for HPHT solutions

- the unicity of spectrum uid, export_filename and xmu_filename inside an experiment is checked.

For the phases, there is a specific ods file, in which you can describe molecules, molions and solids.

NOTES:
======
- In experiments_structure_file_writer, the variable parameters cannot be "spectral_range" -ie the edge- because the generation
	of the title experiment is more complexe in that case.

'''

import sys
import os
from lxml import etree
import unicodedata
from copy import deepcopy
import pyexcel

#======================================================================================================================#
#                                                 RESOLUTION TABLE                                                     #
#======================================================================================================================#
resol = {}
resol['ATOM_Ti'] = ['0.255','NULL']
resol['ATOM_V'] = ['0.280','NULL']
resol['ATOM_Cr'] = ['0.307','NULL']
resol['ATOM_Mn'] = ['0.335','NULL']
resol['ATOM_Fe'] = ['0.365','NULL']
resol['ATOM_Co'] = ['0.395','NULL']
resol['ATOM_Ni'] = ['0.427','NULL']
resol['ATOM_Cu'] = ['0.460','NULL']
resol['ATOM_Zn'] = ['0.495','NULL']
resol['ATOM_Ga'] = ['0.532','NULL']
resol['ATOM_Ge'] = ['0.569','NULL']
resol['ATOM_As'] = ['0.609','NULL']
resol['ATOM_Se'] = ['0.649','NULL']
resol['ATOM_Br'] = ['0.691','NULL']
resol['ATOM_Kr'] = ['0.735','NULL']
resol['ATOM_Rb'] = ['0.779','NULL']
resol['ATOM_Sr'] = ['0.826','NULL']
resol['ATOM_Y'] = ['0.874','NULL']
resol['ATOM_Zr'] = ['0.923','NULL']
resol['ATOM_Nb'] = ['0.973','NULL']
resol['ATOM_Mo'] = ['1.03','NULL']
resol['ATOM_Tc'] = ['1.08','NULL']
resol['ATOM_Ru'] = ['1.13','NULL']
resol['ATOM_Rh'] = ['1.19','NULL']
resol['ATOM_Pd'] = ['1.25','NULL']
resol['ATOM_Ag'] = ['1.30','NULL']
resol['ATOM_Cd'] = ['1.37','NULL']
resol['ATOM_In'] = ['1.43','NULL']
resol['ATOM_Sn'] = ['1.50','NULL']
resol['ATOM_Sb'] = ['1.56','NULL']
resol['ATOM_Te'] = ['1.63','NULL']
resol['ATOM_I'] = ['1.70','NULL']
resol['ATOM_Xe'] = ['1.77','NULL']
resol['ATOM_Cs'] = ['1.85','NULL']
resol['ATOM_Ba'] = ['1.92','0.294']
resol['ATOM_La'] = ['1.99','0.281']
resol['ATOM_Hf'] = ['NULL','0.490']
resol['ATOM_Ta'] = ['NULL','0.507']
resol['ATOM_W'] = ['NULL','0.523']
resol['ATOM_Re'] = ['NULL','0.540']
resol['ATOM_Os'] = ['NULL','0.558']
resol['ATOM_Ir'] = ['NULL','0.575']
resol['ATOM_Pt'] = ['NULL','0.593']
resol['ATOM_Au'] = ['NULL','0.611']
resol['ATOM_Hg'] = ['NULL','0.630']
resol['ATOM_Tl'] = ['NULL','0.649']
resol['ATOM_Pb'] = ['NULL','0.668']
resol['ATOM_Bi'] = ['NULL','0.688']
resol['ATOM_Po'] = ['NULL','0.708']
resol['ATOM_At'] = ['NULL','0.729']
resol['ATOM_Rn'] = ['NULL','0.750']
resol['ATOM_Fr'] = ['NULL','0.771']
resol['ATOM_Ra'] = ['NULL','0.792']
resol['ATOM_Ac'] = ['NULL','0.814']
resol['ATOM_Ce'] = ['NULL','0.293']
resol['ATOM_Pr'] = ['NULL','0.306']
resol['ATOM_Nd'] = ['NULL','0.318']
resol['ATOM_Pm'] = ['NULL','0.331']
resol['ATOM_Sm'] = ['NULL','0.344']
resol['ATOM_Eu'] = ['NULL','0.357']
resol['ATOM_Gd'] = ['NULL','0.371']
resol['ATOM_Tb'] = ['NULL','0.385']
resol['ATOM_Dy'] = ['NULL','0.399']
resol['ATOM_Ho'] = ['NULL','0.414']
resol['ATOM_Er'] = ['NULL','0.428']
resol['ATOM_Tm'] = ['NULL','0.443']
resol['ATOM_Yb'] = ['NULL','0.458']
resol['ATOM_Lu'] = ['NULL','0.474']
resol['ATOM_Th'] = ['NULL','0.836']
resol['ATOM_Pa'] = ['NULL','0.858']
resol['ATOM_U'] = ['NULL','0.880']

#======================================================================================================================#
#                                            ODS TO CSV FILE CONVERTER                                                 #
#======================================================================================================================#
def ods_converter(expdir,odsfilename):
	'''
	ods to csv converter.
	--> use the excel file.
	--> keep all cells with the text format.
	--> duplicate the necessary tabs, keep the tab names just adding a specific suffix.
	--> give 2 arguments: data directory and ods file
	'''
	os.chdir(expdir)

	if odsfilename not in os.listdir():
		sys.exit("File "+odsfilename+" doesn't exist!")

	book_dict = pyexcel.get_book_dict(file_name=odsfilename)

	for elem in book_dict.items():
		if elem[0].startswith('labo') or elem[0].startswith('exper') or elem[0].startswith('pellet') or elem[0].startswith('frozen')\
			or elem[0].startswith('HPHT') or elem[0].startswith('simple') or elem[0].startswith('multiple')\
			or elem[0].startswith('molion') or elem[0].startswith('molec') or elem[0].startswith('solid') or elem[0].startswith('miner'):
			tmpfilename = "tmp.csv"
			outputfilename = elem[0] + ".csv"
			pyexcel.save_as(array=elem[1:],dest_file_name= tmpfilename, dest_delimiter='\n')

			outputfile = open(outputfilename,'w')
			with open(tmpfilename,'r') as f:
				for l in f:
					l=l.split('[\'')[1]
					l=l.split('\']')[0]+'\n'
					l=l.replace('\', \'','\t')
					l=l.replace('\', \"\"','\t')
					l=l.replace('\"\", \'','\t')
					l=l.replace('\"','')
					outputfile.write(l)
			outputfile.close()
			print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                   EXPERIMENTALIST                                                    #
#======================================================================================================================#
def experimentalist_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for experimentalist files.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''

	xmlfile = templatedir +'/'+ "experimentalist.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				labs=[]
				nlabs=-1
				for l in f:
					if str(l.split("\t")[0]).startswith("laboratory_uid"):
						nlabs+=1
						labs.append({})
						labs[nlabs]['laboratory_uid']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("state"):
						labs[nlabs]['state']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("status"):
						labs[nlabs]['status']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("date_begin"):
						labs[nlabs]['date_begin']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("date_end"):
						labs[nlabs]['date_end']=str(l.split("\t")[1])
					else:
						if str(l.split("\t")[0])!='':
							csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Checking the validity of the input file
	check_lines_th = 5 + 5*(nlabs+1)
	check_lines_eff=len(csvdata)-1
	for nlab in range(len(labs)):
		check_lines_eff=check_lines_eff+len(labs[nlab])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")


	#Building the experimentalist uid
	first_name = csvdata['first_name'].replace(" ", "")
	first_name = first_name.replace("-", "")
	first_name = unicodedata.normalize('NFD', first_name)
	first_name = first_name.encode('ascii', 'ignore')
	first_name = first_name.decode("utf-8")

	family_name = csvdata['family_name'].replace(" ", "")
	family_name = family_name.replace("-", "")
	family_name = unicodedata.normalize('NFD', family_name)
	family_name = family_name.encode('ascii', 'ignore')
	family_name = family_name.decode("utf-8")

	csvdata['experimentalist_uid']='EXPER_'+first_name+'_'+family_name

	#Building the acronym
	name_words = csvdata['first_name'] +"_"+ csvdata['family_name']
	name_words = name_words.replace("-", "_")
	name_words = name_words.replace(" ", "_")
	name_words = name_words.split("_")

	acronym = ''
	for word in name_words:
		acronym = acronym + word[0]

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="experimentalist_"+family_name+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Check what you want to do!!!")
					correctanswer=True
					samename=False
					exitprog=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True
	if exitprog:
		sys.exit("This user already exists. Check what you want to do!!!")

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('experimentalist'):
		detail.find('uid').text=csvdata['experimentalist_uid']
		detail.find('import_mode').text=csvdata['experimentalist_import_mode']
		detail.find('first_name').text=csvdata['first_name']
		detail.find('family_name').text=csvdata['family_name']
		detail.find('acronym').text=acronym
		if len(csvdata['orcid_identifier']):
			detail.find('orcid_identifier').text=csvdata['orcid_identifier']
		else:
			detail.find('orcid_identifier').text="NULL"
		if len(csvdata['email']):
			detail.find('email').text=csvdata['email']
		else:
			detail.find('email').text="NULL"

	#Rewriting the xml (elements of the levels 1 and more)
	for item in root.getchildren():
		level1=item.getchildren()
		for n1 in range(len(level1)):
			if level1[n1].tag=='laboratories':
				for n in range(nlabs):
					#adding as many blocks as nlabs
					level1[n1].append( deepcopy(level1[n1][0]) )
				nlab=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					item.attrib['state']=labs[nlab]['state']
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							level2[n2].text=labs[nlab]['laboratory_uid']
						if level2[n2].tag=='status':
							level2[n2].text=labs[nlab]['status']
						if level2[n2].tag=='date_begin':
							level2[n2].text=labs[nlab]['date_begin']
						if level2[n2].tag=='date_end':
							if len(labs[nlab]['date_end']):
								level2[n2].text=labs[nlab]['date_end']
							else:
								level2[n2].text='NULL'
							nlab+=1


	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="experimentalist" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="experimentalist" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                      LABORATORY                                                      #
#======================================================================================================================#
def laboratory_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for laboratory files.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''

	xmlfile = templatedir +'/'+ "laboratory.xml"
	xml=open(xmlfile,'r',encoding='UTF-8')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				orgs=[]
				norgs=-1
				for l in f:
					if str(l.split("\t")[0]).startswith("organization_acronym"):
						norgs+=1
						orgs.append({})
						orgs[norgs]['organization_acronym']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("organization_name"):
						orgs[norgs]['organization_name']=str(l.split("\t")[1])
					else:
						if str(l.split("\t")[0])!='':
							csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Checking the validity of the input file
	check_lines_th = 11 + 2*(norgs+1)
	check_lines_eff=len(csvdata)-1
	for norg in range(len(orgs)):
		check_lines_eff=check_lines_eff+len(orgs[norg])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="laboratory_"+csvdata['acronym']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Check what you want to do!!!")
					correctanswer=True
					samename=False
					exitprog=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True
	if exitprog:
		sys.exit("This laboratory already exists. Check what you want to do!!!")

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('laboratory'):
		detail.find('uid').text=csvdata['laboratory_uid']
		detail.find('import_mode').text=csvdata['laboratory_import_mode']
		detail.find('acronym').text=csvdata['acronym']
		detail.find('name').text="<![CDATA["+csvdata['name']+"]]>"

	#Rewriting the xml (elements of the levels 1 and more)
	for item in root.getchildren():
		level1=item.getchildren()
		for n1 in range(len(level1)):
			if level1[n1].tag=='addresses':
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='street':
							level2[n2].text=csvdata['street']
						if level2[n2].tag=='postal_code':
							level2[n2].text=csvdata['postal_code']
						if level2[n2].tag=='city':
							level2[n2].text=csvdata['city']
						if level2[n2].tag=='region':
							level2[n2].text=csvdata['region']
						if level2[n2].tag=='country_code':
							level2[n2].text=csvdata['country_code']
			if level1[n1].tag=='links':
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='name':
							level2[n2].text=csvdata['link_name']
						if level2[n2].tag=='url':
							level2[n2].text=csvdata['link_url']
			if level1[n1].tag=='organizations':
				for n in range(norgs):
					#adding as many blocks as norgs
					level1[n1].append( deepcopy(level1[n1][0]) )
				norg=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='acronym':
							level2[n2].text=orgs[norg]['organization_acronym']
						if level2[n2].tag=='name':
							level2[n2].text="<![CDATA["+orgs[norg]['organization_name']+"]]>"
							norg+=1


	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="laboratory" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="laboratory" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                SAMPLE: PELLET TYPE                                                   #
#======================================================================================================================#
def sample_pellet_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for sample files describing a pellet.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''
	xmlfile = templatedir +'/'+ "sample_pellet.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename='tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	exps=[]
	nexps=-1
	pubs=[]
	npubs=-1
	flag='std'
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile) as f:
				for l in f:
					if str(l.split("\t")[0]).startswith("experimentalist_uid"):
						flag='expdef'
						nexps+=1
					if str(l.split("\t")[0]).startswith("publication_uid"):
						flag='pubdef'
						npubs+=1
					if flag=='std' and str(l.split("\t")[0])!='':
						csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
					if flag=='expdef' and str(l.split("\t")[0])!='':
						exps.append(str(l.split("\t")[1]))
					if flag=='pubdef' and str(l.split("\t")[0])!='':
						pubs.append(str(l.split("\t")[1]))
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Building the sample name
	sample_name='Pellet of '+csvdata['material_name']
	if csvdata['diluent']!='NULL':
		sample_name=sample_name+' mixed with '+csvdata['diluent']
	sample_name='<![CDATA['+sample_name+']]>'

	#Building the diluent uid
	if csvdata['diluent']=='BN':
		diluent_uid='MATMIN_BN_DT_20161208_001'
	if csvdata['diluent']=='cellulose':
		diluent_uid='MATMIN_Cellulose_IK_20181119_001'
	if csvdata['diluent']=='PVP':
		diluent_uid='MATMIN_PVP_IK_20210920_001'


	#Checking the validity of the input file
	check_lines_th = 16 + (nexps+1) + (npubs+1)
	check_lines_eff = len(csvdata)-1 + (nexps+1) + (npubs+1)
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="sample_"+csvdata['sample_uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Please type another name")
					csvdata['sample_uid']=input("sample uid (Ex: OP_20180115_005): ")
					correctanswer=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('sample'):
		detail.find('uid').text='SAMPLE_'+csvdata['sample_uid']
		detail.find('import_mode').text=csvdata['sample_import_mode']
		detail.find('date').text=csvdata['sample_date']
		detail.find('name').text=sample_name
		if len(csvdata['sample_provider']):
			detail.find('provider').text=csvdata['sample_provider']
		else:
			detail.find('provider').text="NULL"

	#Rewriting the xml (elements of the levels 1 and more)

	for item in root.getchildren():
		level1=item.getchildren()

		for n1 in range(len(level1)):
			if level1[n1].tag=='experimentalists':
				for n in range(nexps):
					#adding as many blocks as nexps
					level1[n1].append( deepcopy(level1[n1][1]) ) #level1[n1][1] (and not [0]) because of the comment
				nexp=0
				for elem in level1[n1].findall("experimentalist_uid"):
					if exps[nexp]=="":
						sys.exit("ERROR: You should have at least one experimentalist for "+outputfilename+".")
					if exps[nexp].split("_")[0]!="EXPER":
						sys.exit("ERROR: Your experimentalist_uid "+exps[nexp]+" is not valid.")
					else:
						elem.text=exps[nexp]
					nexp+=1

		for n1 in range(len(level1)):
			if level1[n1].tag=='layers':
				for item1 in level1[n1].getchildren():
					level2=item1.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='import_mode':
							level2[n2].text=csvdata['sample_import_mode']
						if level2[n2].tag=='name':
							level2[n2].text=sample_name
						if level2[n2].tag=='matters':
							for item in level2[n2].getchildren():
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='uid' and csvdata['diluent']!='NULL':
										level3[n3].text=diluent_uid
									if level3[n3].tag=='mass':
										if csvdata['diluent_mass']=="" and csvdata['diluent']!='NULL':
											sys.exit("ERROR: You should specify the diluent mass for "+outputfilename+ \
										". Put NULL if you don't know.")
										else:
											level3[n3].text=csvdata['diluent_mass']
									if level3[n3].tag=='mass_error':
										if len(csvdata['diluent_mass_error']):
											level3[n3].text=csvdata['diluent_mass_error']
										else:
											level3[n3].text="NULL"
						if level2[n2].tag=='materials':
							for item in level2[n2].getchildren():
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='uid':
										level3[n3].text='MATERIAL_'+csvdata['sample_uid']
									if level3[n3].tag=='import_mode':
										level3[n3].text=csvdata['sample_import_mode']
									if level3[n3].tag=='mass':
										if csvdata['material_mass']=="":
											sys.exit("ERROR: You should specify the material mass for "+outputfilename+ \
										". Put NULL if you don't know.")
										else:
											level3[n3].text=csvdata['material_mass']
									if level3[n3].tag=='mass_error':
										if len(csvdata['material_mass_error']):
											level3[n3].text=csvdata['material_mass_error']
										else:
											level3[n3].text="NULL"
									if level3[n3].tag=='name':
										level3[n3].text='<![CDATA['+csvdata['material_name']+']]>'
									if level3[n3].tag=='origin':
										level3[n3].text=csvdata['material_origin']
									if level3[n3].tag=='grain':
										for item in level3[n3].getchildren():
											level4=item.getchildren()
											for n4 in range(len(level4)):
												if level4[n4].tag=='size':
													for elem in level4[n4].findall('median'):
														elem.text=csvdata['grain_size_median']
													for elem in level4[n4].findall('min'):
														elem.text=csvdata['grain_size_min']
													for elem in level4[n4].findall('max'):
														elem.text=csvdata['grain_size_max']
									if level3[n3].tag=='comments':
										level3[n3].text=csvdata['comments']
									if level3[n3].tag=='publications':
										for n in range(npubs):
											#adding as many blocks as npubs
											level3[n3].append( deepcopy(level3[n3][0]) )
										npub=0
										for elem in level3[n3].findall("publication_uid"):
											elem.text=pubs[npub]
											npub+=1
									if level3[n3].tag=='basic_constituents':
										for item in level3[n3].getchildren():
											level4=item.getchildren()
											for n4 in range(len(level4)):
												if level4[n4].tag=='uid':
													level4[n4].text='CONST_'+csvdata['sample_uid']
												if level4[n4].tag=='import_mode':
													level4[n4].text=csvdata['sample_import_mode']
												if level4[n4].tag=='name':
													level4[n4].text='<![CDATA['+csvdata['material_name']+']]>'
												if level4[n4].tag=='fundamental_phase_uid':
													level4[n4].text=csvdata['fundamental_phase_uid']
					#Removing the matters block when there is no diluent
					if csvdata['diluent']=='NULL':
						for elem in item1.getchildren():
							if elem.tag=="matters":
								item1.remove(elem)

		for n1 in range(len(level1)):
			if level1[n1].tag=='processings':
				for elem in level1[n1].findall("title"):
					elem.text='<![CDATA[mixing and compressing '+csvdata['material_name']+']]>'
					if csvdata['diluent']!='NULL':
						elem.text=elem.text+' with '+csvdata['diluent']
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='matter_uid' and csvdata['diluent']!='NULL':
							level2[n2].text=diluent_uid
						if level2[n2].tag=='material_uid':
							level2[n2].text='MATERIAL_'+csvdata['sample_uid']
				for elem in level1[n1].findall("product_sample_uid"):
					elem.text='SAMPLE_'+csvdata['sample_uid']
				#Removing the matters block when there is no diluent
				if csvdata['diluent']=='NULL':
					for item in level1[n1].getchildren():
						if item.tag=="precursor_matters":
							level1[n1].remove(item)

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="sample" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="sample" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")



#======================================================================================================================#
#                                            SAMPLE: FROZEN SOLUTION TYPE                                              #
#======================================================================================================================#
def sample_frozen_solution_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for sample files describing a frozen solution.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''
	xmlfile = templatedir +'/'+ "sample_frozen_solution.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename='tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	exps=[]
	nexps=-1
	pubs=[]
	npubs=-1
	spes=[]
	nspes=-1
	flag='std'
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				for l in f:
					if str(l.split("\t")[0]).startswith("experimentalist_uid"):
						flag='expdef'
						nexps+=1
					if str(l.split("\t")[0]).startswith("publication_uid"):
						flag='pubdef'
						npubs+=1
					if str(l.split("\t")[0]).startswith("specie_uid"):
						flag='spedef'
						spes.append({})
						nspes+=1

					if flag=='std' and str(l.split("\t")[0])!='':
						csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
					if flag=='expdef' and str(l.split("\t")[0])!='':
						exps.append(str(l.split("\t")[1]))
					if flag=='pubdef' and str(l.split("\t")[0])!='':
						pubs.append(str(l.split("\t")[1]))
					if flag=='spedef' and str(l.split("\t")[0])!='':
						spes[nspes][str(l.split("\t")[0])]=str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Building the sample name
	sample_name='<![CDATA[Frozen solution of '+csvdata['material_name']+']]>'

	#Checking the validity of the input file
	#Nb common lines + nb experimentalists + nb publications + (nb specie lines)*(nb species)
	check_lines_th = 8 + (nexps+1) + (npubs+1) + 5*(nspes+1)

	check_lines_eff= len(csvdata)-1 + (nexps+1) + (npubs+1)
	for nspe in range(len(spes)):
		check_lines_eff=check_lines_eff+(len(spes[nspe]))

	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="sample_"+csvdata['sample_uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Please type another name")
					csvdata['sample_uid']=input("sample uid (Ex: OP_20180115_005): ")
					correctanswer=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('sample'):
		detail.find('uid').text='SAMPLE_'+csvdata['sample_uid']
		detail.find('import_mode').text=csvdata['sample_import_mode']
		detail.find('date').text=csvdata['sample_date']
		detail.find('name').text=sample_name
		if len(csvdata['sample_provider']):
			detail.find('provider').text=csvdata['sample_provider']
		else:
			detail.find('provider').text="NULL"

	#Rewriting the xml (elements of the levels 1 and more)

	for item in root.getchildren():
		level1=item.getchildren()

		for n1 in range(len(level1)):
			if level1[n1].tag=='experimentalists':
				for n in range(nexps):
					#adding as many blocks as nexps
					level1[n1].append( deepcopy(level1[n1][1]) ) #level1[n1][1] (and not [0]) because of the comment
				nexp=0
				for elem in level1[n1].findall("experimentalist_uid"):
					if exps[nexp]=="":
						sys.exit("ERROR: You should have at least one experimentalist for "+outputfilename+".")
					if exps[nexp].split("_")[0]!="EXPER":
						sys.exit("ERROR: Your experimentalist_uid "+exps[nexp]+" is not valid.")
					else:
						elem.text=exps[nexp]
					nexp+=1

		for n1 in range(len(level1)):
			if level1[n1].tag=='layers':
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='import_mode':
							level2[n2].text=csvdata['sample_import_mode']
						if level2[n2].tag=='name':
							level2[n2].text=sample_name
						if level2[n2].tag=='materials':
							for item in level2[n2].getchildren():
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='uid':
										level3[n3].text='MATERIAL_'+csvdata['sample_uid']
									if level3[n3].tag=='import_mode':
										level3[n3].text=csvdata['sample_import_mode']
									if level3[n3].tag=='name':
										level3[n3].text=sample_name
									if level3[n3].tag=='origin':
										level3[n3].text=csvdata['material_origin']
									if level3[n3].tag=='comments':
										level3[n3].text='<![CDATA['+csvdata['comments']+']]>'
									if level3[n3].tag=='publications':
										for n in range(npubs):
											#adding as many blocks as npubs
											level3[n3].append( deepcopy(level3[n3][0]) )
										npub=0
										for elem in level3[n3].findall("publication_uid"):
											elem.text=pubs[npub]
											npub+=1
									if level3[n3].tag=='constituents':
										for item in level3[n3].getchildren():
											level4=item.getchildren()
											for n4 in range(len(level4)):
												if level4[n4].tag=='uid':
													level4[n4].text='CONST_'+csvdata['sample_uid']
												if level4[n4].tag=='import_mode':
													level4[n4].text=csvdata['sample_import_mode']
												if level4[n4].tag=='name':
													level4[n4].text=sample_name
												if level4[n4].tag=='species_mole_fraction_unit':
													if csvdata['specie_quantity_unit']=='g' or csvdata['specie_quantity_unit']=='mol'\
													or csvdata['specie_quantity_unit']=='none':
														level4[n4].text='NULL'
													else:
														level4[n4].text=csvdata['specie_quantity_unit']
												if level4[n4].tag=='species':
													for n in range(nspes):
														#adding as many blocks as nspes
														level4[n4].append( deepcopy(level4[n4][0]) )
													nspe=0
													for item in level4[n4].getchildren():
														level5=item.getchildren()
														for n5 in range(len(level5)):
															if level5[n5].tag=='family':
																if spes[nspe]['specie_uid'].startswith("MOLEC") or \
																spes[nspe]['specie_uid'].startswith("MOLION"):
																	level5[n5].text='molecule'
																if spes[nspe]['specie_uid'].startswith("ATOM") or \
																spes[nspe]['specie_uid'].startswith("ATION"):
																	level5[n5].text='element'
															if level5[n5].tag=='uid':
																level5[n5].text=spes[nspe]['specie_uid']
															if level5[n5].tag=='state':
																level5[n5].text=spes[nspe]['specie_state']
															if level5[n5].tag=='relevance':
																level5[n5].text=spes[nspe]['specie_relevance']
															if level5[n5].tag=='mass':
																level5[n5].text=spes[nspe]['specie_quantity']
															if level5[n5].tag=='mole':
																level5[n5].text=spes[nspe]['specie_quantity']
															if level5[n5].tag=='mole_fraction':
																level5[n5].text=spes[nspe]['specie_quantity']
															if level5[n5].tag=='comments':
																level5[n5].text='<![CDATA['+spes[nspe]['specie_comments']+']]>'
														if spes[nspe]['specie_quantity']=="":
															sys.exit("ERROR: You should specify the specie quantity for "+outputfilename+ \
														". Put NULL if you don't know.")
														nspe+=1

													for item in level4[n4].getchildren():
														for nspe in range(len(spes)):
															if csvdata['specie_quantity_unit']=='mol':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mass"):
																			item.remove(elem)
																		for elem in item.findall("mass_error"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction_error"):
																			item.remove(elem)
															if csvdata['specie_quantity_unit']=='g':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mole"):
																			item.remove(elem)
																		for elem in item.findall("mole_error"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction_error"):
																			item.remove(elem)
															if csvdata['specie_quantity_unit']=='mol/kg' or csvdata['specie_quantity_unit']=='mol/l':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mass"):
																			item.remove(elem)
																		for elem in item.findall("mass_error"):
																			item.remove(elem)
																		for elem in item.findall("mole"):
																			item.remove(elem)
																		for elem in item.findall("mole_error"):
																			item.remove(elem)
															if csvdata['specie_quantity_unit']=='none':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mass"):
																			elem.text="NULL"
																		for elem in item.findall("mass_error"):
																			elem.text="NULL"
																		for elem in item.findall("mole"):
																			item.remove(elem)
																		for elem in item.findall("mole_error"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction_error"):
																			item.remove(elem)


	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')


	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="sample" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="sample" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")


#======================================================================================================================#
#                                             SAMPLE: HP HT SOLUTION TYPE                                              #
#======================================================================================================================#
def sample_HPHT_solution_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for sample files describing a HP/HT solution.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''
	xmlfile = templatedir +'/'+ "sample_HPHT_solution.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename='tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	exps=[]
	nexps=-1
	pubs=[]
	npubs=-1
	spes=[]
	nspes=-1
	flag='std'
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				for l in f:
					if str(l.split("\t")[0]).startswith("experimentalist_uid"):
						flag='expdef'
						nexps+=1
					if str(l.split("\t")[0]).startswith("publication_uid"):
						flag='pubdef'
						npubs+=1
					if str(l.split("\t")[0]).startswith("specie_uid"):
						flag='spedef'
						spes.append({})
						nspes+=1

					if flag=='std' and str(l.split("\t")[0])!='':
						csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
					if flag=='expdef' and str(l.split("\t")[0])!='':
						exps.append(str(l.split("\t")[1]))
					if flag=='pubdef' and str(l.split("\t")[0])!='':
						pubs.append(str(l.split("\t")[1]))
					if flag=='spedef' and str(l.split("\t")[0])!='':
						spes[nspes][str(l.split("\t")[0])]=str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Checking the validity of the input file
	#Nb common lines + nb experimentalists + nb publications + (nb specie lines)*(nb species)
	check_lines_th = 8 + (nexps+1) + (npubs+1) + 5*(nspes+1)

	check_lines_eff= len(csvdata)-1 + (nexps+1) + (npubs+1)
	for nspe in range(len(spes)):
		check_lines_eff=check_lines_eff+(len(spes[nspe]))

	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="sample_"+csvdata['sample_uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Please type another name")
					csvdata['sample_uid']=input("sample uid (Ex: OP_20180115_005): ")
					correctanswer=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('sample'):
		detail.find('uid').text='SAMPLE_'+csvdata['sample_uid']
		detail.find('import_mode').text=csvdata['sample_import_mode']
		detail.find('date').text=csvdata['sample_date']
		detail.find('name').text='<![CDATA['+csvdata['sample_name']+']]>'
		if len(csvdata['sample_provider']):
			detail.find('provider').text=csvdata['sample_provider']
		else:
			detail.find('provider').text="NULL"
		if csvdata['sample_volume']=="":
			sys.exit("ERROR: You should specify the sample volume for "+outputfilename+ \
		". Put NULL if you don't know.")
		else:
			detail.find('volume').text=csvdata['sample_volume']

	#Rewriting the xml (elements of the levels 1 and more)

	for item in root.getchildren():
		level1=item.getchildren()

		for n1 in range(len(level1)):
			if level1[n1].tag=='experimentalists':
				for n in range(nexps):
					#adding as many blocks as nexps
					level1[n1].append( deepcopy(level1[n1][1]) ) #level1[n1][1] (and not [0]) because of the comment
				nexp=0
				for elem in level1[n1].findall("experimentalist_uid"):
					if exps[nexp]=="":
						sys.exit("ERROR: You should have at least one experimentalist for "+outputfilename+".")
					if exps[nexp].split("_")[0]!="EXPER":
						sys.exit("ERROR: Your experimentalist_uid "+exps[nexp]+" is not valid.")
					else:
						elem.text=exps[nexp]
					nexp+=1

			if level1[n1].tag=='publications':
				for n in range(npubs):
					#adding as many blocks as npubs
					level1[n1].append( deepcopy(level1[n1][0]) )
				npub=0
				for elem in level1[n1].findall("publication_uid"):
					elem.text=pubs[npub]
					npub+=1

			if level1[n1].tag=='layers':
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='import_mode':
							level2[n2].text=csvdata['sample_import_mode']
						if level2[n2].tag=='name':
							level2[n2].text='<![CDATA['+csvdata['sample_name']+']]>'
						if level2[n2].tag=='density':
							if len(csvdata['solution_density']):
								level2[n2].text=csvdata['solution_density']
							else:
								level2[n2].text="NULL"
						if level2[n2].tag=='materials':
							for item in level2[n2].getchildren():
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='uid':
										level3[n3].text='MATERIAL_'+csvdata['sample_uid']
									if level3[n3].tag=='name':
										level3[n3].text='<![CDATA['+csvdata['sample_name']+']]>'
									if level3[n3].tag=='import_mode':
										level3[n3].text=csvdata['sample_import_mode']
									if level3[n3].tag=='constituents':
										for item in level3[n3].getchildren():
											level4=item.getchildren()
											for n4 in range(len(level4)):
												if level4[n4].tag=='uid':
													level4[n4].text='CONST_'+csvdata['sample_uid']
												if level4[n4].tag=='name':
													level4[n4].text='<![CDATA['+csvdata['sample_name']+']]>'
												if level4[n4].tag=='import_mode':
													level4[n4].text=csvdata['sample_import_mode']
												if level4[n4].tag=='species_mole_fraction_unit':
													if csvdata['specie_quantity_unit']=='g' or csvdata['specie_quantity_unit']=='mol'\
													or csvdata['specie_quantity_unit']=='none':
														level4[n4].text='NULL'
													else:
														level4[n4].text=csvdata['specie_quantity_unit']
												if level4[n4].tag=='species':
													for n in range(nspes):
														#adding as many blocks as nspes
														level4[n4].append( deepcopy(level4[n4][0]) )
													nspe=0
													for item in level4[n4].getchildren():
														level5=item.getchildren()
														for n5 in range(len(level5)):
															if level5[n5].tag=='family':
																if spes[nspe]['specie_uid'].startswith("MOLEC") or \
																spes[nspe]['specie_uid'].startswith("MOLION"):
																	level5[n5].text='molecule'
																if spes[nspe]['specie_uid'].startswith("ATOM") or \
																spes[nspe]['specie_uid'].startswith("ATION"):
																	level5[n5].text='element'
															if level5[n5].tag=='uid':
																level5[n5].text=spes[nspe]['specie_uid']
															if level5[n5].tag=='state':
																level5[n5].text=spes[nspe]['specie_state']
															if level5[n5].tag=='relevance':
																level5[n5].text=spes[nspe]['specie_relevance']
															if level5[n5].tag=='mass':
																level5[n5].text=spes[nspe]['specie_quantity']
															if level5[n5].tag=='mole':
																level5[n5].text=spes[nspe]['specie_quantity']
															if level5[n5].tag=='mole_fraction':
																level5[n5].text=spes[nspe]['specie_quantity']
															if level5[n5].tag=='comments':
																level5[n5].text='<![CDATA['+spes[nspe]['specie_comments']+']]>'
														if spes[nspe]['specie_quantity']=="":
															sys.exit("ERROR: You should specify the specie quantity for "+outputfilename+ \
														". Put NULL if you don't know.")
														nspe+=1

													for item in level4[n4].getchildren():
														for nspe in range(len(spes)):
															if csvdata['specie_quantity_unit']=='mol':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mass"):
																			item.remove(elem)
																		for elem in item.findall("mass_error"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction_error"):
																			item.remove(elem)
															if csvdata['specie_quantity_unit']=='g':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mole"):
																			item.remove(elem)
																		for elem in item.findall("mole_error"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction_error"):
																			item.remove(elem)
															if csvdata['specie_quantity_unit']=='mol/kg' or csvdata['specie_quantity_unit']=='mol/l':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mass"):
																			item.remove(elem)
																		for elem in item.findall("mass_error"):
																			item.remove(elem)
																		for elem in item.findall("mole"):
																			item.remove(elem)
																		for elem in item.findall("mole_error"):
																			item.remove(elem)
															if csvdata['specie_quantity_unit']=='none':
																for elem in item.findall("uid"):
																	if elem.text==spes[nspe]['specie_uid']:
																		for elem in item.findall("mass"):
																			elem.text="NULL"
																		for elem in item.findall("mass_error"):
																			elem.text="NULL"
																		for elem in item.findall("mole"):
																			item.remove(elem)
																		for elem in item.findall("mole_error"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction"):
																			item.remove(elem)
																		for elem in item.findall("mole_fraction_error"):
																			item.remove(elem)

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="sample" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="sample" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")


#======================================================================================================================#
#                               EXPERIMENTS_SPECTRA WITH MAXIMUM ONE VARIABLE PARAMETER                                #
#======================================================================================================================#
def experiments_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for experiments_spectra files describing 1 or more spectra (1 variable condition: instrument or pressure).
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''
	xmlfile = templatedir +'/'+ "experiments_spectra.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	exps=[]
	nexps=-1
	pubs=[]
	npubs=-1
	inss=[]
	ninss=-1
	flag='std'
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				for l in f:
					if str(l.split("\t")[0]).startswith("experimentalist_uid"):
						flag='expdef'
						nexps+=1
					if str(l.split("\t")[0]).startswith("instrument_uid"):
						flag='insdef'
						inss.append({})
						ninss+=1
					if str(l.split("\t")[0]).startswith("publication_uid"):
						flag='pubdef'
						npubs+=1
					if flag=='std' and str(l.split("\t")[0])!='':
						csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
					if flag=='pubdef' and str(l.split("\t")[0])!='':
						pubs.append(str(l.split("\t")[1]))
					if flag=='expdef' and str(l.split("\t")[0])!='':
						exps.append(str(l.split("\t")[1]))
					if flag=='insdef' and str(l.split("\t")[0])!='':
						inss[ninss][str(l.split("\t")[0])]=str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Checking the validity of the input file
	check_lines_th=8+nexps+npubs+28*(ninss+1)
	check_lines_eff=len(csvdata)-1+nexps+npubs
	for nins in range(len(inss)):
		check_lines_eff=check_lines_eff+len(inss[nins])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Defining the beamline (if only simulated data, put None, but if at least one experimental spectrum, put the BL name.) and the type of experiment.
	BLid=None
	real_exp=False
	sim_exp=False
	for nins in range(len(inss)):
		BLacronym=inss[nins]['instrument_uid'].split("_")[-1]
		if BLacronym=='FAME':
			BLid='BM30'
			real_exp=True
		if BLacronym=='FAMEUHD':
			BLid='BM16'
			real_exp=True
		if BLacronym=='FDMNES':
			sim_exp=True
	exptypes=[]
	if real_exp:
		exptypes.append('laboratory measurement')
	if sim_exp:
		exptypes.append('theoretical modeling')
	

	#Building the title of the experiment
		##No variable parameter --> put all info at once
	if csvdata['variable_parameters_type']=='no':
		title=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+\
		" edge "
		if inss[0]['measurement_type']=='XES':
			title=title+inss[0]['measurement_type']+' '
		else:
			title=title+'XAS '+inss[0]['measurement_type']+' '
		title=title+"of "+csvdata['experiment_title']

		##Variable parameters = observation mode --> add the info of the measurement types (avoiding double citing)
	if csvdata['variable_parameters_type']=='observation mode':
		title=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
		flag_XES=1
		flag_XAS=[]
		for nins in range(len(inss)):
			if inss[nins]['measurement_type']=='XES':
				if flag_XES:
					if nins!=0:
						title=title+'and '
					title=title+inss[nins]['measurement_type']+' '
					flag_XES=0
			else:
				if inss[nins]['measurement_type'] not in flag_XAS:
					if nins!=0:
						title=title+'and '
					title=title+'XAS '+inss[nins]['measurement_type']+' '
					flag_XAS.append(inss[nins]['measurement_type'])
		title=title+"of "+csvdata['experiment_title']

		##Variable parameters = spectral range (ie the edge) --> add the info of the edges (avoiding double citing)
	if csvdata['variable_parameters_type']=='spectral range':
		title=''
		flag_edge_elem=[]
		flag_edge_type=[]
		for nins in range(len(inss)):
			if inss[nins]['absorption_edge_element_uid'] not in flag_edge_elem or inss[nins]['absorption_edge_type'] not in flag_edge_type:
				if nins!=0:
					title=title+'and '
				title=title+inss[nins]['absorption_edge_element_uid'].split("_")[1]+" "+inss[nins]['absorption_edge_type']+' edge '
				flag_edge_elem.append(inss[nins]['absorption_edge_element_uid'])
				flag_edge_type.append(inss[nins]['absorption_edge_type'])
		if inss[0]['measurement_type']=='XES':
			title=title+inss[0]['measurement_type']+' '
		else:
			title=title+'XAS '+inss[0]['measurement_type']+' '
		title=title+"of "+csvdata['experiment_title']

		##Variable parameters = pressure or temperature or sample composition --> add the info of the measurement conditions
		##"other" refers to the sample_atmosphere. Check should be necessary if this use evolves.
	if csvdata["variable_parameters_type"]=='temperature' or csvdata["variable_parameters_type"]=='pressure' \
	or csvdata["variable_parameters_type"]=='sample composition' or csvdata["variable_parameters_type"]=='other':
		title=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
		if inss[0]['measurement_type']=='XES':
			title=title+inss[0]['measurement_type']+' '
		else:
			title=title+'XAS '+inss[0]['measurement_type']+' '
		title=title+"of "+csvdata['experiment_title']

	title='<![CDATA['+title+']]>'

	#Building the titles of the spectra
	for nins in range(len(inss)):
		inss[nins]['built_spectrum_title']=inss[nins]['absorption_edge_element_uid'].split("_")[1]+" "+\
		inss[nins]['absorption_edge_type']+" edge "
		if inss[nins]['measurement_type']=='XES':
			inss[nins]['built_spectrum_title']=inss[nins]['built_spectrum_title']+inss[nins]['measurement_type']+" "
		else:
			inss[nins]['built_spectrum_title']=inss[nins]['built_spectrum_title']+'XAS '+inss[nins]['measurement_type']+" "
		inss[nins]['built_spectrum_title']='<![CDATA['+inss[nins]['built_spectrum_title']+"of "+inss[nins]['spectrum_title']+']]>'

	#Building the comment on calibration/resolution
	for nins in range(len(inss)):
		if inss[nins]['instrument_uid'].split("_")[-1]=='FDMNES':
			inss[nins]['calibration_comment']="<![CDATA[The resolution of the calculated spectrum is given by the convolution parameter. \
The energy was shifted to the value of the absorption edge of a reference: "+inss[nins]["calibration_material"]+"("+inss[nins]["calibration_energy"]+" eV)."
		else:
			inss[nins]['calibration_comment']="<![CDATA[The resolution in the above table is the resolution \
given by the monochromator. The energy was calibrated by measuring the \
absorption edge of a reference: "+inss[nins]["calibration_material"]+" (first \
maximum of the derivative set at "+inss[nins]["calibration_energy"]+" eV)."			

		if len(inss[nins]["CAS_resolution"])!=0:
			inss[nins]['calibration_comment']=inss[nins]['calibration_comment']+" The resolution of the spectrometer analyser was "\
			+inss[nins]["CAS_resolution"]+" eV (FWHM of the elastic peak)."

		inss[nins]['calibration_comment']=inss[nins]['calibration_comment']+"]]>"

	#Checking the validity of the ouput file name
	valid=False
	while not valid:
		outputfilename="experiments_spectra_"+csvdata['experiment_uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Please type another name")
					csvdata['experiment_uid']=input("experiment uid (Ex: OP_20180115_005): ")
					correctanswer=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('experiment'):
		detail.find('uid').text="EXPERIMENT_"+csvdata['experiment_uid']
		if BLid:
			#means data is not simulated spectra
			detail.find('comments').text="<![CDATA[These data were acquired on the "+BLid+" beamline in the frame of the "+csvdata['proposal_number']+" proposal.]]>"
		else:
			detail.find('comments').text=""
		detail.find('import_mode').text=csvdata['experiment_import_mode']
		detail.find('title').text=title
		detail.find('date_begin').text=csvdata['experiment_date_begin']
		detail.find('date_end').text=csvdata['experiment_date_end']

	#Rewriting the xml (elements of the levels 1 and more)

	for item in root.getchildren():
		level1=item.getchildren()

		for n1 in range(len(level1)):
			if level1[n1].tag=='experimentalists':
				for n in range(nexps):
					#adding as many blocks as nexps
					level1[n1].append( deepcopy(level1[n1][1]) ) #level1[n1][1] (and not [0]) because of the comment
				nexp=0
				for elem in level1[n1].findall("experimentalist_uid"):
					if exps[nexp]=="":
						sys.exit("ERROR: You should have at least one experimentalist for "+outputfilename+".")
					if exps[nexp].split("_")[0]!="EXPER":
						sys.exit("ERROR: Your experimentalist_uid "+exps[nexp]+" is not valid.")
					else:
						elem.text=exps[nexp]
					nexp+=1

			if level1[n1].tag=='types':
				for n in range(len(exptypes)-1):
					#adding as many blocks as experimental types
					level1[n1].append( deepcopy(level1[n1][0]) )
				nexptype=0
				for elem in level1[n1].findall("type"):
					elem.text=exptypes[nexptype]
					nexptype+=1

			if level1[n1].tag=='publications':
				for n in range(npubs):
					#adding as many blocks as npubs
					level1[n1].append( deepcopy(level1[n1][0]) )
				npub=0
				for elem in level1[n1].findall("publication_uid"):
					if pubs[npub]=="":
						sys.exit("ERROR: You should have at least one publication for the experiment. If not, put 'NULL' in the publication_uid field.")
					if pubs[npub].split("_")[0]!="PUBLI" and pubs[npub].split("_")[0]!="NULL":
						sys.exit("ERROR: Your publication_uid is not valid.")
					elem.text=pubs[npub]
					npub+=1

			if level1[n1].tag=='variable_parameters_types':
				for elem in level1[n1].findall("variable_parameters_type"):
					elem.text=csvdata["variable_parameters_type"]

			if level1[n1].tag=='parameters_instruments':
				#If variable_parameters_type is observation mode or spectral range, add as many blocks as ninss
				#Else, the instrument is described only once
				if csvdata["variable_parameters_type"]=='observation mode' or csvdata["variable_parameters_type"]=='spectral range':
					for n in range(ninss):
						level1[n1].append( deepcopy(level1[n1][0]) )
				nins=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='instrument_uid':
							level2[n2].text=inss[nins]['instrument_uid']
						if level2[n2].tag=='instrument_sample_holder':
							level2[n2].text='<![CDATA['+inss[nins]['instrument_sample_holder']+']]>'
						if level2[n2].tag=='spectral':
							for item in level2[n2].getchildren():
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='range':
										for elem in level3[n3].findall("min"):
											elem.text=inss[nins]['min_energy']
										for elem in level3[n3].findall("max"):
											elem.text=inss[nins]['max_energy']
										for elem in level3[n3].findall("absorption_edge_element_uid"):
											elem.text=inss[nins]['absorption_edge_element_uid']
										for elem in level3[n3].findall("absorption_edge_type"):
											elem.text=inss[nins]['absorption_edge_type']
										for elem in level3[n3].findall("resolution"):
											if str(inss[nins]['instrument_uid']).split("_")[2]=='FDMNES':
												elem.text=""
											elif inss[nins]['absorption_edge_type']=='K':
												elem.text=resol[inss[nins]['absorption_edge_element_uid']][0]
											elif inss[nins]['absorption_edge_type'].startswith('L'):
												elem.text=resol[inss[nins]['absorption_edge_element_uid']][1]
							for elem in level2[n2].findall("comments"):
								elem.text=inss[nins]['calibration_comment']
						if level2[n2].tag=='angle':
							for elem in level2[n2].findall("observation_geometry"):
								#autimatic filling as a function of the instrument
								if inss[nins]['measurement_type']=='fluorescence'\
								or inss[nins]['measurement_type']=='HERFD'\
								or inss[nins]['measurement_type']=='XES':
									elem.text='directional'
								if inss[nins]['measurement_type']=='transmission':
									elem.text='direct'
							for elem in level2[n2].findall("comments"):
								#autimatic filling as a function of the instrument
								if str(inss[nins]['instrument_uid']).split("_")[2]=='CAS'\
								or str(inss[nins]['instrument_uid']).split("_")[2]=='FLUO':
									elem.text='<![CDATA[Emitted x-rays are collected in a solid angle directed at 90 degrees from the incident beam.]]>'
						if level2[n2].tag=='spatial':
							for elem in level2[n2].findall("observation_mode"):
								elem.text=csvdata["spatial_observation_mode"]
					nins+=1

			if level1[n1].tag=='spectra':
				for n in range(ninss):
					#adding as many blocks as ninss
					level1[n1].append( deepcopy(level1[n1][0]) )
				nins=0
				spectrum_uid_list=[]
				export_filename_list=[]
				xmu_filename_list=[]
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							if inss[nins]['spectrum_uid'] in spectrum_uid_list:
								sys.exit("ERROR: spectrum_uid "+inss[nins]['spectrum_uid']+" already exists.")
							else:
								level2[n2].text='SPECTRUM_'+inss[nins]['spectrum_uid']
								spectrum_uid_list.append(inss[nins]['spectrum_uid'])
						if level2[n2].tag=='import_mode':
							level2[n2].text=csvdata['experiment_import_mode']
						if level2[n2].tag=='experiment_type':
							if str(inss[nins]['instrument_uid']).split("_")[2]=='FDMNES':
								level2[n2].text='theoretical modeling'
						if level2[n2].tag=='title':
							level2[n2].text=inss[nins]['built_spectrum_title']
						if level2[n2].tag=='type':
							#autimatic filling as a function of the instrument
							if inss[nins]['measurement_type']=='fluorescence'\
							or inss[nins]['measurement_type']=='HERFD'\
							or inss[nins]['measurement_type']=='XES':
								level2[n2].text='fluorescence emission'
							if inss[nins]['measurement_type']=='transmission':
								#level2[n2].text='transmission'
								level2[n2].text='absorbance'
						if level2[n2].tag=='sample':
							for elem in level2[n2].findall("uid"):
								elem.text='SAMPLE_'+inss[nins]['sample_uid']
						if level2[n2].tag=='parameters_environment':
							for item in level2[n2].findall("temperature"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									#if level3[n3].tag=='unit':
										#level3[n3].text=inss[nins]['temperature_unit']
									if level3[n3].tag=='value':
										level3[n3].text=inss[nins]['temperature_value']
									if level3[n3].tag=='error':
										level3[n3].text=inss[nins]['temperature_error']
									if level3[n3].tag=='time':
										level3[n3].text=inss[nins]['duration']
							for item in level2[n2].findall("pressure"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									#if level3[n3].tag=='unit':
										#level3[n3].text=inss[nins]['pressure_unit']
									if level3[n3].tag=='value':
										level3[n3].text=inss[nins]['pressure_value']
									if level3[n3].tag=='error':
										level3[n3].text=inss[nins]['pressure_error']
									if level3[n3].tag=='time':
										level3[n3].text=inss[nins]['duration']
							for item in level2[n2].findall("fluid"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if inss[nins]['sample_atmosphere']=='helium':
										if level3[n3].tag=='type':
											level3[n3].text='atomic gas'
										if level3[n3].tag=='temperature':
											level3[n3].text=inss[nins]['temperature_value']
										if level3[n3].tag=='pressure':
											level3[n3].text=inss[nins]['pressure_value']
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text='ATOM_He'
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=str(1)
									if inss[nins]['sample_atmosphere']=='H2':
										if level3[n3].tag=='type':
											level3[n3].text='molecular gas'
										if level3[n3].tag=='temperature':
											level3[n3].text=inss[nins]['temperature_value']
										if level3[n3].tag=='pressure':
											level3[n3].text=inss[nins]['pressure_value']
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text='MOLEC_H2'
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=str(1)
									if inss[nins]['sample_atmosphere']=='nitrogen':
										if level3[n3].tag=='type':
											level3[n3].text='atomic gas'
										if level3[n3].tag=='temperature':
											level3[n3].text=inss[nins]['temperature_value']
										if level3[n3].tag=='pressure':
											level3[n3].text=inss[nins]['pressure_value']
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text='MOLEC_N2'
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=str(1)
									if inss[nins]['sample_atmosphere']=='ambient air' or inss[nins]['sample_atmosphere']=='vacuum'\
									or inss[nins]['sample_atmosphere']=='NULL':
										if level3[n3].tag=='type':
											level3[n3].text=inss[nins]['sample_atmosphere']
										if level3[n3].tag=='temperature':
											level3[n3].text='NULL'
										if level3[n3].tag=='pressure':
											level3[n3].text='NULL'
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text=''
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=''
						if level2[n2].tag=='parameters_instruments':
							for item in level2[n2].findall("parameters_instrument"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='instrument_uid':
										level3[n3].text=inss[nins]['instrument_uid']
									if level3[n3].tag=='spectral':
										for elem in level3[n3].findall("comments"):
											elem.text='<![CDATA['+inss[nins]['spectrum_comments']+']]>'
											
						if level2[n2].tag=='date_begin':
							level2[n2].text=inss[nins]['spectrum_date']
						if level2[n2].tag=='publications':
							for n in range(npubs):
								#adding as many blocks as npubs
								level2[n2].append( deepcopy(level2[n2][0]) )
							npub=0
							for elem in level2[n2].findall("publication_uid"):
								elem.text=pubs[npub]
						if level2[n2].tag=='files':
							for item in level2[n2].findall("parameter"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=="header_lines_number":
										level3[n3].text=inss[nins]['header_lines_number']
									if level3[n3].tag=="column_total_number":
										level3[n3].text=inss[nins]['column_total_number']
									if level3[n3].tag=="column_separator":
										level3[n3].text=inss[nins]['column_separator']
									if level3[n3].tag=="columns":
										col_nb=1
										for item in level3[n3].findall("column"):
											level4=item.getchildren()
											if col_nb==1:
												for n4 in range(len(level4)):
													if level4[n4].tag=="number":
														level4[n4].text=inss[nins]['energy_column_number']
											if col_nb==2:
												for n4 in range(len(level4)):
													if level4[n4].tag=="number":
														level4[n4].text=inss[nins]['intensity_column_number']
											col_nb+=1
							for item in level2[n2].findall("file"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=="filename":
										if inss[nins]['xmu_filename'] in xmu_filename_list:
											sys.exit("ERROR: xmu_filename "+inss[nins]['xmu_filename']+" already exists.")
										else:
											level3[n3].text=inss[nins]['xmu_filename']
											xmu_filename_list.append(inss[nins]['xmu_filename'])

						if level2[n2].tag=="export_filename":
							if inss[nins]['export_filename'] in export_filename_list:
								sys.exit("ERROR: export_filename "+inss[nins]['export_filename']+" already exists.")
							else:
								level2[n2].text=inss[nins]['export_filename']
								export_filename_list.append(inss[nins]['export_filename'])

					nins+=1

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="experiment_spectra" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="experiment_spectra" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")


#======================================================================================================================#
#                                     EXPERIMENTS_SPECTRA WITH TWO VARIABLE PARAMETERS                                 #
#======================================================================================================================#
def experiments_structure_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for experiments_spectra files describing several organised spectra with 2 variable conditions:
	instrument, sample composition, temperature, pressure or atmosphere (implemented as "other").
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''
	xmlfile = templatedir +'/'+ "experiments_spectra_with_structure.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	exps=[]
	nexps=-1
	inss=[]
	ninss=-1
	pubs=[]
	npubs=-1
	pars=[]
	tits=[]
	flag='std'
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				for l in f:
					if str(l.split("\t")[0]).startswith("experimentalist_uid"):
						flag='expdef'
						nexps+=1
					if str(l.split("\t")[0]).startswith("instrument_uid"):
						flag='insdef'
						inss.append({})
						ninss+=1
					if str(l.split("\t")[0]).startswith("publication_uid"):
						flag='pubdef'
						npubs+=1
					if str(l.split("\t")[0]).startswith("variable_parameters_type"):
						flag='pardef'
					if str(l.split("\t")[0]).startswith("subexperiment_title"):
						flag='titdef'

					if flag=='std' and str(l.split("\t")[0])!='':
						csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
					if flag=='expdef' and str(l.split("\t")[0])!='':
						exps.append(str(l.split("\t")[1]))
					if flag=='insdef' and str(l.split("\t")[0])!='':
						inss[ninss][str(l.split("\t")[0])]=str(l.split("\t")[1])
					if flag=='pubdef' and str(l.split("\t")[0])!='':
						pubs.append(str(l.split("\t")[1]))
					if flag=='pardef' and str(l.split("\t")[0])!='':
						pars.append(str(l.split("\t")[1]))
					if flag=='titdef' and str(l.split("\t")[0])!='':
						tits.append(str(l.split("\t")[1]))
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Building the structure
		##Definition of the varying parameters
	if pars[0]=='observation mode':
		varpar1='instrument_uid'
	if pars[0]=='pressure':
		varpar1='pressure_value'
	if pars[0]=='sample composition':
		varpar1='sample_uid'
	if pars[0]=='temperature':
		varpar1='temperature_value'
	if pars[0]=='spectral range':
		varpar1='absorption_edge_element_uid'
	if pars[0]=='other':
		varpar1='sample_atmosphere'
		#This case should be more detailled if "other" is used for another variation than atmosphere

	if pars[1]=='observation mode':
		varpar2='instrument_uid'
	if pars[1]=='pressure':
		varpar2='pressure_value'
	if pars[1]=='sample composition':
		varpar2='sample_uid'
	if pars[1]=='temperature':
		varpar2='temperature_value'
	if pars[0]=='other':
		varpar1='sample_atmosphere'
		#This case should be more detailled if "other" is used for another variation than atmosphere

		##Filling vals, a list of lists [[param1,param2, param2,...],[param1,param2, param2,...],...]
	vals=[]
	for nin in range(len(inss)):
		if not inss[nin][varpar1] in (vals[x][0] for x in range(len(vals))):
			val=[]
			val.append(inss[nin][varpar1])
			for nins in range(len(inss)):
				if inss[nins][varpar1]==val[0]:
					if not inss[nins][varpar2] in val[1:]:
						val.append(inss[nins][varpar2])
			vals.append(val)

	#Defining the beamline (if only simulated data, put None, but if at least one experimental spectrum, put the BL name.) and the type of experiment.
	BLid=None
	real_exp=False
	sim_exp=False
	for nins in range(len(inss)):
		BLacronym=inss[nins]['instrument_uid'].split("_")[-1]
		if BLacronym=='FAME':
			BLid='BM30'
			real_exp=True
		if BLacronym=='FAMEUHD':
			BLid='BM16'
			real_exp=True
		if BLacronym=='FDMNES':
			sim_exp=True
	exptypes=[]
	if real_exp:
		exptypes.append('laboratory measurement')
	if sim_exp:
		exptypes.append('theoretical modeling')

	#Building the title of the experiment
	if 'observation mode' in pars:
		title=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
		flag_XES=1
		flag_XAS=[]
		for nins in range(len(inss)):
			if inss[nins]['measurement_type']=='XES':
				if flag_XES:
					if nins!=0:
						title=title+'and '
					title=title+inss[nins]['measurement_type']+' '
					flag_XES=0
			else:
				if inss[nins]['measurement_type'] not in flag_XAS:
					if nins!=0:
						title=title+'and '
					title=title+'XAS '+inss[nins]['measurement_type']+' '
					flag_XAS.append(inss[nins]['measurement_type'])
		title=title+"of "+csvdata['experiment_title']		
	else:
		title=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
		if inss[0]['measurement_type']=='XES':
			title=title+inss[0]['measurement_type']+' '
		else:
			title=title+'XAS '+inss[0]['measurement_type']+' '
		title=title+"of "+csvdata['experiment_title']

	title='<![CDATA['+title+']]>'

	#Building the titles of the sub-experiments
	meas_type_index=0
	for ntit in range(len(tits)):
		if pars[0]=='observation mode':
			subtitle=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
			if inss[meas_type_index]['measurement_type']=='XES':
				subtitle=subtitle+inss[meas_type_index]['measurement_type']+' '
			else:
				subtitle=subtitle+'XAS '+inss[meas_type_index]['measurement_type']+' '
			subtitle=subtitle+"of "+tits[ntit]
		elif pars[0]=='spectral range':
			subtitle=inss[meas_type_index]['absorption_edge_element_uid'].split("_")[1]+" "+inss[meas_type_index]['absorption_edge_type']+" edge "
			if inss[meas_type_index]['measurement_type']=='XES':
				subtitle=subtitle+inss[meas_type_index]['measurement_type']+' '
			else:
				subtitle=subtitle+'XAS '+inss[meas_type_index]['measurement_type']+' '
			subtitle=subtitle+"of "+tits[ntit]
		else:
			if pars[1]=='observation mode':
				subtitle=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
				if inss[meas_type_index]['measurement_type']=='XES':
					subtitle=subtitle+' '
				else:
					subtitle=subtitle+'XAS '
				subtitle=subtitle+"of "+tits[ntit]
			elif pars[1]=='spectral range':
				subtitle=inss[meas_type_index]['measurement_type']+' '
				subtitle=subtitle+"of "+tits[ntit]
			else:
				subtitle=inss[0]['absorption_edge_element_uid'].split("_")[1]+" "+inss[0]['absorption_edge_type']+" edge "
				if inss[meas_type_index]['measurement_type']=='XES':
					subtitle=subtitle+inss[meas_type_index]['measurement_type']+' '
				else:
					subtitle=subtitle+'XAS '+inss[meas_type_index]['measurement_type']+' '
				subtitle=subtitle+"of "+tits[ntit]
		subtitle='<![CDATA['+subtitle+']]>'
		vals[ntit].append(subtitle)
		meas_type_index+=len(vals[ntit])-2

	#Building the titles of the spectra
	for nins in range(len(inss)):
		inss[nins]['built_spectrum_title']=inss[nins]['absorption_edge_element_uid'].split("_")[1]+" "+\
		inss[nins]['absorption_edge_type']+" edge "
		if inss[nins]['measurement_type']=='XES':
			inss[nins]['built_spectrum_title']=inss[nins]['built_spectrum_title']+inss[nins]['measurement_type']+" "
		else:
			inss[nins]['built_spectrum_title']=inss[nins]['built_spectrum_title']+'XAS '+inss[nins]['measurement_type']+" "
		inss[nins]['built_spectrum_title']='<![CDATA['+inss[nins]['built_spectrum_title']+"of "+inss[nins]['spectrum_title'] +']]>'

	#Building the comment on calibration/resolution
	for nins in range(len(inss)):
		if inss[nins]['instrument_uid'].split("_")[-1]=='FDMNES':
			inss[nins]['calibration_comment']="<![CDATA[The resolution of the calculated spectrum is given by the convolution parameter. \
The energy was shifted to the value of the absorption edge of a reference: "+inss[nins]["calibration_material"]+"("+inss[nins]["calibration_energy"]+" eV)."
		else:
			inss[nins]['calibration_comment']="<![CDATA[The resolution in the above table is the resolution \
given by the monochromator. The energy was calibrated by measuring the \
absorption edge of a reference: "+inss[nins]["calibration_material"]+" (first \
maximum of the derivative set at "+inss[nins]["calibration_energy"]+" eV)."

		if len(inss[nins]["CAS_resolution"])!=0:
			inss[nins]['calibration_comment']=inss[nins]['calibration_comment']+" The resolution of the spectrometer analyser was "\
			+inss[nins]["CAS_resolution"]+" eV (FWHM of the elastic peak)."

		inss[nins]['calibration_comment']=inss[nins]['calibration_comment']+"]]>"

	#Checking the validity of the input file
	check_lines_th = 10 + (nexps+1) + (npubs+1) + 28*(ninss+1) + len(vals)
	check_lines_eff = len(csvdata)-1 + len(pars) + (nexps+1) + (npubs+1) + len(tits)
	for nins in range(len(inss)):
		check_lines_eff=check_lines_eff+len(inss[nins])-2 #-2 because 2 lines added for built_spectrum_title
															#and for calibration_comment
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	while not valid:
		outputfilename="experiments_spectra_"+csvdata['experiment_uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Please type another name")
					csvdata['experiment_uid']=input("experiment uid (Ex: OP_20180115_005): ")
					correctanswer=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('experiment'):
		detail.find('uid').text="EXPERIMENT_"+csvdata['experiment_uid']
		if BLid:
			#means data is not simulated spectra
			detail.find('comments').text="<![CDATA[These data were acquired on the "+BLid+" beamline in the frame of the "+csvdata['proposal_number']+" proposal.]]>"
		else:
			detail.find('comments').text=""
		detail.find('import_mode').text=csvdata['experiment_import_mode']
		detail.find('title').text=title
		detail.find('description').text=csvdata['description']
		detail.find('date_begin').text=csvdata['experiment_date_begin']
		detail.find('date_end').text=csvdata['experiment_date_end']

	#Rewriting the xml (elements of the levels 1 and more)

	for item in root.getchildren():
		level1=item.getchildren()

		for n1 in range(len(level1)):
			if level1[n1].tag=='experimentalists':
				for n in range(nexps):
					#adding as many blocks as nexps
					level1[n1].append( deepcopy(level1[n1][1]) ) #level1[n1][1] (and not [0]) because of the comment
				nexp=0
				for elem in level1[n1].findall("experimentalist_uid"):
					if exps[nexp]=="":
						sys.exit("ERROR: You should have at least one experimentalist for "+outputfilename+".")
					if exps[nexp].split("_")[0]!="EXPER":
						sys.exit("ERROR: Your experimentalist_uid "+exps[nexp]+" is not valid.")
					else:
						elem.text=exps[nexp]
					nexp+=1

			if level1[n1].tag=='types':
				for n in range(len(exptypes)-1):
					#adding as many blocks as experimental types
					level1[n1].append( deepcopy(level1[n1][0]) )
				nexptype=0
				for elem in level1[n1].findall("type"):
					elem.text=exptypes[nexptype]
					nexptype+=1

			if level1[n1].tag=='variable_parameters_types':
				npar=0
				for elem in level1[n1].findall("variable_parameters_type"):
					elem.text=pars[npar]
					npar+=1

			if level1[n1].tag=='publications':
				for n in range(npubs):
					#adding as many blocks as npubs
					level1[n1].append( deepcopy(level1[n1][0]) )
				npub=0
				for elem in level1[n1].findall("publication_uid"):
					if pubs[npub]=="":
						sys.exit("ERROR: You should have at least one publication for the experiment. If not, put 'NULL' in the publication_uid field.")
					if pubs[npub].split("_")[0]!="PUBLI" and pubs[npub].split("_")[0]!="NULL":
						sys.exit("ERROR: Your publication_uid is not valid.")
					elem.text=pubs[npub]
					npub+=1

			if level1[n1].tag=='parameters_instruments':
				#If variable_parameters_type is observation mode or spectral range, add as many blocks as different instruments or different spectral ranges
				#Else, the instrument is described only once
				## Observation mode case
				if 'observation mode' in pars:
					inslist=[]
					insidx=[]
					for nins in range(len(inss)):
						if inss[nins]['instrument_uid'] not in inslist:
							inslist.append(inss[nins]['instrument_uid'])
							insidx.append(nins)
					for n in range(len(inslist)-1):
						level1[n1].append( deepcopy(level1[n1][0]) )
					nins=0
					for item in level1[n1].getchildren():
						level2=item.getchildren()
						for n2 in range(len(level2)):
							if level2[n2].tag=='instrument_uid':
								level2[n2].text=inss[insidx[nins]]['instrument_uid']
							if level2[n2].tag=='instrument_sample_holder':
								level2[n2].text='<![CDATA['+inss[insidx[nins]]['instrument_sample_holder']+']]>'
							if level2[n2].tag=='spectral':
								for item in level2[n2].getchildren():
									level3=item.getchildren()
									for n3 in range(len(level3)):
										if level3[n3].tag=='range':
											for elem in level3[n3].findall("min"):
												elem.text=inss[insidx[nins]]['min_energy']
											for elem in level3[n3].findall("max"):
												elem.text=inss[insidx[nins]]['max_energy']
											for elem in level3[n3].findall("absorption_edge_element_uid"):
												elem.text=inss[insidx[nins]]['absorption_edge_element_uid']
											for elem in level3[n3].findall("absorption_edge_type"):
												elem.text=inss[insidx[nins]]['absorption_edge_type']
											for elem in level3[n3].findall("resolution"):
												if str(inss[nins]['instrument_uid']).split("_")[2]=='FDMNES':
													elem.text=""
												elif inss[nins]['absorption_edge_type']=='K':
													elem.text=resol[inss[nins]['absorption_edge_element_uid']][0]
												elif inss[nins]['absorption_edge_type'].startswith('L'):
													elem.text=resol[inss[nins]['absorption_edge_element_uid']][1]
								for elem in level2[n2].findall("comments"):
									elem.text=inss[insidx[nins]]['calibration_comment']
							if level2[n2].tag=='angle':
								for elem in level2[n2].findall("observation_geometry"):
									#autimatic filling as a function of the instrument
									if inss[nins]['measurement_type']=='fluorescence'\
									or inss[nins]['measurement_type']=='HERFD'\
									or inss[nins]['measurement_type']=='XES':
										elem.text='directional'
									if inss[nins]['measurement_type']=='transmission':
										elem.text='direct'
								for elem in level2[n2].findall("comments"):
									#autimatic filling as a function of the instrument
									if str(inss[insidx[nins]]['instrument_uid']).split("_")[2]=='CAS'\
									or str(inss[insidx[nins]]['instrument_uid']).split("_")[2]=='FLUO':
										elem.text='<![CDATA[Emitted x-rays are collected in a solid angle directed at 90 degrees from the incident beam.]]>'
							if level2[n2].tag=='spatial':
								for elem in level2[n2].findall("observation_mode"):
									elem.text=csvdata["spatial_observation_mode"]
						nins+=1
					
				## Spectral range case
				elif 'spectral range' in pars:
					sprlist=[]
					spridx=[]
					for nins in range(len(inss)):
						spr=inss[nins]['absorption_edge_type']+'_'+inss[nins]['absorption_edge_element_uid']
						if spr not in sprlist:
							sprlist.append(spr)
							spridx.append(nins)
					for n in range(len(sprlist)-1):
						level1[n1].append( deepcopy(level1[n1][0]) )
					nins=0
					for item in level1[n1].getchildren():
						level2=item.getchildren()
						for n2 in range(len(level2)):
							if level2[n2].tag=='instrument_uid':
								level2[n2].text=inss[spridx[nins]]['instrument_uid']
							if level2[n2].tag=='instrument_sample_holder':
								level2[n2].text='<![CDATA['+inss[spridx[nins]]['instrument_sample_holder']+']]>'
							if level2[n2].tag=='spectral':
								for item in level2[n2].getchildren():
									level3=item.getchildren()
									for n3 in range(len(level3)):
										if level3[n3].tag=='range':
											for elem in level3[n3].findall("min"):
												elem.text=inss[spridx[nins]]['min_energy']
											for elem in level3[n3].findall("max"):
												elem.text=inss[spridx[nins]]['max_energy']
											for elem in level3[n3].findall("absorption_edge_element_uid"):
												elem.text=inss[spridx[nins]]['absorption_edge_element_uid']
											for elem in level3[n3].findall("absorption_edge_type"):
												elem.text=inss[spridx[nins]]['absorption_edge_type']
											for elem in level3[n3].findall("resolution"):
												if str(inss[nins]['instrument_uid']).split("_")[2]=='FDMNES':
													elem.text=""
												elif inss[nins]['absorption_edge_type']=='K':
													elem.text=resol[inss[nins]['absorption_edge_element_uid']][0]
												elif inss[nins]['absorption_edge_type'].startswith('L'):
													elem.text=resol[inss[nins]['absorption_edge_element_uid']][1]
								for elem in level2[n2].findall("comments"):
									elem.text=inss[spridx[nins]]['calibration_comment']
							if level2[n2].tag=='angle':
								for elem in level2[n2].findall("observation_geometry"):
									#autimatic filling as a function of the instrument
									if inss[nins]['measurement_type']=='fluorescence'\
									or inss[nins]['measurement_type']=='HERFD'\
									or inss[nins]['measurement_type']=='XES':
										elem.text='directional'
									if inss[nins]['measurement_type']=='transmission':
										elem.text='direct'
								for elem in level2[n2].findall("comments"):
									#autimatic filling as a function of the instrument
									if str(inss[spridx[nins]]['instrument_uid']).split("_")[2]=='CAS'\
									or str(inss[spridx[nins]]['instrument_uid']).split("_")[2]=='FLUO':
										elem.text='<![CDATA[Emitted x-rays are collected in a solid angle directed at 90 degrees from the incident beam.]]>'
							if level2[n2].tag=='spatial':
								for elem in level2[n2].findall("observation_mode"):
									elem.text=csvdata["spatial_observation_mode"]
						nins+=1
				## Cases where the instrument is described only once
				else:
					nins=0
					insidx=[]
					insidx.append(nins)
					for item in level1[n1].getchildren():
						level2=item.getchildren()
						for n2 in range(len(level2)):
							if level2[n2].tag=='instrument_uid':
								level2[n2].text=inss[insidx[nins]]['instrument_uid']
							if level2[n2].tag=='instrument_sample_holder':
								level2[n2].text='<![CDATA['+inss[insidx[nins]]['instrument_sample_holder']+']]>'
							if level2[n2].tag=='spectral':
								for item in level2[n2].getchildren():
									level3=item.getchildren()
									for n3 in range(len(level3)):
										if level3[n3].tag=='range':
											for elem in level3[n3].findall("min"):
												elem.text=inss[insidx[nins]]['min_energy']
											for elem in level3[n3].findall("max"):
												elem.text=inss[insidx[nins]]['max_energy']
											for elem in level3[n3].findall("absorption_edge_element_uid"):
												elem.text=inss[insidx[nins]]['absorption_edge_element_uid']
											for elem in level3[n3].findall("absorption_edge_type"):
												elem.text=inss[insidx[nins]]['absorption_edge_type']
											for elem in level3[n3].findall("resolution"):
												if str(inss[nins]['instrument_uid']).split("_")[2]=='FDMNES':
													elem.text=""
												elif inss[nins]['absorption_edge_type']=='K':
													elem.text=resol[inss[nins]['absorption_edge_element_uid']][0]
												elif inss[nins]['absorption_edge_type'].startswith('L'):
													elem.text=resol[inss[nins]['absorption_edge_element_uid']][1]
								for elem in level2[n2].findall("comments"):
									elem.text=inss[insidx[nins]]['calibration_comment']
							if level2[n2].tag=='angle':
								for elem in level2[n2].findall("observation_geometry"):
									#autimatic filling as a function of the instrument
									if inss[nins]['measurement_type']=='fluorescence'\
									or inss[nins]['measurement_type']=='HERFD'\
									or inss[nins]['measurement_type']=='XES':
										elem.text='directional'
									if inss[nins]['measurement_type']=='transmission':
										elem.text='direct'
								for elem in level2[n2].findall("comments"):
									#autimatic filling as a function of the instrument
									if str(inss[insidx[nins]]['instrument_uid']).split("_")[2]=='CAS'\
									or str(inss[insidx[nins]]['instrument_uid']).split("_")[2]=='FLUO':
										elem.text='<![CDATA[Emitted x-rays are collected in a solid angle directed at 90 degrees from the incident beam.]]>'
							if level2[n2].tag=='spatial':
								for elem in level2[n2].findall("observation_mode"):
									elem.text=csvdata["spatial_observation_mode"]
					
			#Writting the structure
			if level1[n1].tag=='structure':
				for item in level1[n1].getchildren():
					if item.tag=='sections':
						item.attrib['variable_parameter']=pars[0]
					for n in range(len(vals)-1):
						#adding as many blocks as varpar1
						item.append(deepcopy(item.findall("section")[0]))
					level2=item.getchildren()
					nvar=0
					for n2 in range(len(level2)):
						if level2[n2].tag=='section':
							for item2 in level2[n2].getchildren():
								if item2.tag=='title':
									item2.text=vals[nvar][-1]
								if item2.tag=='spectra':
									item2.attrib['variable_parameter']=pars[1]
									for n in range(len(vals[nvar])-3):
										#adding as many blocks as varpar2 (-3 because vals[nvar] contains param1 and subtitle)
										item2.append(deepcopy(item2.findall("spectrum_uid")[0]))
							nvar+=1

			#Filling the structure
			nins=0
			spectrum_uid_list=[]
			export_filename_list=[]
			xmu_filename_list=[]
			if level1[n1].tag=='structure':
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='section':
							for item2 in level2[n2].getchildren():
								if item2.tag=='spectra':
									for elem in item2.findall("spectrum_uid"):
										if inss[nins]['spectrum_uid'] in spectrum_uid_list:
											sys.exit("ERROR: spectrum_uid "+inss[nins]['spectrum_uid']+" already exists.")
										else:
											elem.text='SPECTRUM_'+inss[nins]['spectrum_uid']
											spectrum_uid_list.append(inss[nins]['spectrum_uid'])

										nins+=1


			if level1[n1].tag=='spectra':
				for n in range(ninss):
					#adding as many blocks as ninss
					level1[n1].append( deepcopy(level1[n1][0]) )
				nins=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							level2[n2].text='SPECTRUM_'+inss[nins]['spectrum_uid']
						if level2[n2].tag=='import_mode':
							level2[n2].text=csvdata['experiment_import_mode']
						if level2[n2].tag=='experiment_type':
							if str(inss[nins]['instrument_uid']).split("_")[2]=='FDMNES':
								level2[n2].text='theoretical modeling'
						if level2[n2].tag=='title':
							level2[n2].text=inss[nins]['built_spectrum_title']
						if level2[n2].tag=='type':
							#autimatic filling as a function of the instrument
							if inss[nins]['measurement_type']=='fluorescence'\
							or inss[nins]['measurement_type']=='HERFD'\
							or inss[nins]['measurement_type']=='XES':
								level2[n2].text='fluorescence emission'
							if inss[nins]['measurement_type']=='transmission':
								#level2[n2].text='transmission'
								level2[n2].text='absorbance'
						if level2[n2].tag=='sample':
							for elem in level2[n2].findall("uid"):
								elem.text='SAMPLE_'+inss[nins]['sample_uid']
						if level2[n2].tag=='parameters_environment':
							for item in level2[n2].findall("temperature"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									#if level3[n3].tag=='unit':
										#level3[n3].text=inss[nins]['temperature_unit']
									if level3[n3].tag=='value':
										level3[n3].text=inss[nins]['temperature_value']
									if level3[n3].tag=='error':
										level3[n3].text=inss[nins]['temperature_error']
									if level3[n3].tag=='time':
										level3[n3].text=inss[nins]['duration']
							for item in level2[n2].findall("pressure"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									#if level3[n3].tag=='unit':
										#level3[n3].text=inss[nins]['pressure_unit']
									if level3[n3].tag=='value':
										level3[n3].text=inss[nins]['pressure_value']
									if level3[n3].tag=='error':
										level3[n3].text=inss[nins]['pressure_error']
									if level3[n3].tag=='time':
										level3[n3].text=inss[nins]['duration']
							for item in level2[n2].findall("fluid"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if inss[nins]['sample_atmosphere']=='helium':
										if level3[n3].tag=='type':
											level3[n3].text='atomic gas'
										if level3[n3].tag=='temperature':
											level3[n3].text=inss[nins]['temperature_value']
										if level3[n3].tag=='pressure':
											level3[n3].text=inss[nins]['pressure_value']
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text='ATOM_He'
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=str(1)
									if inss[nins]['sample_atmosphere']=='H2':
										if level3[n3].tag=='type':
											level3[n3].text='atomic gas'
										if level3[n3].tag=='temperature':
											level3[n3].text=inss[nins]['temperature_value']
										if level3[n3].tag=='pressure':
											level3[n3].text=inss[nins]['pressure_value']
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text='MOLEC_H2'
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=str(1)
									if inss[nins]['sample_atmosphere']=='nitrogen':
										if level3[n3].tag=='type':
											level3[n3].text='atomic gas'
										if level3[n3].tag=='temperature':
											level3[n3].text=inss[nins]['temperature_value']
										if level3[n3].tag=='pressure':
											level3[n3].text=inss[nins]['pressure_value']
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text='MOLEC_N2'
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=str(1)
									if inss[nins]['sample_atmosphere']=='ambient air' or inss[nins]['sample_atmosphere']=='vacuum'\
									or inss[nins]['sample_atmosphere']=='NULL':
										if level3[n3].tag=='type':
											level3[n3].text=inss[nins]['sample_atmosphere']
										if level3[n3].tag=='temperature':
											level3[n3].text='NULL'
										if level3[n3].tag=='pressure':
											level3[n3].text='NULL'
										if level3[n3].tag=='time':
											level3[n3].text=inss[nins]['duration']
										if level3[n3].tag=='composition_species':
											for item in level3[n3].findall("composition_specie"):
												level4=item.getchildren()
												for n4 in range(len(level4)):
													if level4[n4].tag=='uid':
														level4[n4].text=''
													if level4[n4].tag=='mole_fraction':
															level4[n4].text=''
						if level2[n2].tag=='parameters_instruments':
							for item in level2[n2].findall("parameters_instrument"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=='instrument_uid':
										level3[n3].text=inss[nins]['instrument_uid']
									if level3[n3].tag=='spectral':
										for elem in level3[n3].findall("comments"):
											elem.text='<![CDATA['+inss[nins]['spectrum_comments']+']]>'
											
						if level2[n2].tag=='date_begin':
							level2[n2].text=inss[nins]['spectrum_date']

						if level2[n2].tag=='publications':
							for n in range(npubs):
								#adding as many blocks as npubs
								level2[n2].append( deepcopy(level2[n2][0]) )
							npub=0
							for elem in level2[n2].findall("publication_uid"):
								elem.text=pubs[npub]
						if level2[n2].tag=='files':
							for item in level2[n2].findall("parameter"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=="header_lines_number":
										level3[n3].text=inss[nins]['header_lines_number']
									if level3[n3].tag=="column_total_number":
										level3[n3].text=inss[nins]['column_total_number']
									if level3[n3].tag=="column_separator":
										level3[n3].text=inss[nins]['column_separator']
									if level3[n3].tag=="columns":
										col_nb=1
										for item in level3[n3].findall("column"):
											level4=item.getchildren()
											if col_nb==1:
												for n4 in range(len(level4)):
													if level4[n4].tag=="number":
														level4[n4].text=inss[nins]['energy_column_number']
											if col_nb==2:
												for n4 in range(len(level4)):
													if level4[n4].tag=="number":
														level4[n4].text=inss[nins]['intensity_column_number']
											col_nb+=1
							for item in level2[n2].findall("file"):
								level3=item.getchildren()
								for n3 in range(len(level3)):
									if level3[n3].tag=="filename":
										if inss[nins]['xmu_filename'] in xmu_filename_list:
											sys.exit("ERROR: xmu_filename "+inss[nins]['xmu_filename']+" already exists.")
										else:
											level3[n3].text=inss[nins]['xmu_filename']
											xmu_filename_list.append(inss[nins]['xmu_filename'])
						if level2[n2].tag=="export_filename":
							if inss[nins]['export_filename'] in export_filename_list:
								sys.exit("ERROR: export_filename "+inss[nins]['export_filename']+" already exists.")
							else:
								level2[n2].text=inss[nins]['export_filename']
								export_filename_list.append(inss[nins]['export_filename'])
					nins+=1

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="experiment_spectra" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="experiment_spectra" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                           MOLECULE                                                   #
#======================================================================================================================#
def molec_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for molecule-ion (molion) files.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''

	xmlfile = templatedir +'/'+ "molecule.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				names=[]
				nnames=-1
				atoms=[]
				natoms=-1
				links=[]
				nlinks=-1
				for l in f:
					if str(l.split("\t")[0]).startswith("secondary_name"):
						nnames+=1
						names.append({})
						names[nnames]['secondary_name']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("atom_uid"):
						natoms+=1
						atoms.append({})
						atoms[natoms]['atom_uid']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("atom_nb"):
						atoms[natoms]['atom_nb']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("link_name"):
						nlinks+=1
						links.append({})
						links[nlinks]['link_name']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("link_url"):
						links[nlinks]['link_url']=str(l.split("\t")[1])
					else:
						if str(l.split("\t")[0])!='':
							csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Checking the validity of the input file
	check_lines_th = 17 + 1*(nnames+1) + 2*(natoms+1) + 2*(nlinks+1)
	check_lines_eff=len(csvdata)-1
	for nname in range(len(names)):
		check_lines_eff=check_lines_eff+len(names[nname])
	for natom in range(len(atoms)):
		check_lines_eff=check_lines_eff+len(atoms[natom])
	for nlink in range(len(links)):
		check_lines_eff=check_lines_eff+len(links[nlink])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="molecule_"+csvdata['uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Check what you want to do!!!")
					correctanswer=True
					samename=False
					exitprog=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True
	if exitprog:
		sys.exit("This user already exists. Check what you want to do!!!")

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('molecule'):
		detail.find('uid').text='MOLEC_'+csvdata['uid']
		detail.find('import_mode').text=csvdata['import_mode']
		detail.find('name').text=csvdata['name']
		detail.find('iupac_name').text=csvdata['iupac_name']
		detail.find('inchi').text=csvdata['inchi']
		detail.find('inchikey').text=csvdata['inchikey']
		detail.find('cas_number').text=csvdata['cas_number']
		detail.find('formula').text='<![CDATA['+csvdata['formula']+']]>'
		detail.find('chemical_formula').text=csvdata['chemical_formula']
		detail.find('chemical_structure_filename').text=csvdata['chemical_structure_filename']
		detail.find('structural_formula').text='<![CDATA['+csvdata['structural_formula']+']]>'
		detail.find('symmetry').text=csvdata['symmetry']
		detail.find('case').text=csvdata['case']
		detail.find('molar_mass').text=csvdata['molar_mass']
		detail.find('protic').text=csvdata['protic']
		detail.find('polarity').text=csvdata['polarity']
		detail.find('comments').text='<![CDATA['+csvdata['comments']+']]>'

	#Rewriting the xml (elements of the levels 1 and more)
	for item in root.getchildren():
		level1=item.getchildren()
		for n1 in range(len(level1)):
			if level1[n1].tag=='secondary_names':
				for nname in range(nnames):
					#adding as many blocks as nnames
					level1[n1].append( deepcopy(level1[n1][0]) )
				nname=0
				for elem in level1[n1].findall("secondary_name"):
					elem.text=names[nname]['secondary_name']
					nname+=1
			if level1[n1].tag=='atoms':
				for natom in range(natoms):
					#adding as many blocks as natoms
					level1[n1].append( deepcopy(level1[n1][1]) )
				natom=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							level2[n2].text=atoms[natom]['atom_uid']
						if level2[n2].tag=='number':
							level2[n2].text=atoms[natom]['atom_nb']
							natom+=1
			if level1[n1].tag=='links':
				for nlink in range(nlinks):
					#adding as many blocks as nlinks
					level1[n1].append( deepcopy(level1[n1][1]) )
				nlink=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='name':
							level2[n2].text='<![CDATA['+links[nlink]['link_name']+']]>'
						if level2[n2].tag=='url':
							level2[n2].text='<![CDATA['+links[nlink]['link_url']+']]>'
							nlink+=1

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="species" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="species" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                           MOLION                                                     #
#======================================================================================================================#
def molion_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for molecule-ion (molion) files.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''

	xmlfile = templatedir +'/'+ "molecule-ion.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)

	#Reading the csv file and storing the data
	csvdata={}
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				names=[]
				nnames=-1
				atoms=[]
				natoms=-1
				links=[]
				nlinks=-1
				for l in f:
					if str(l.split("\t")[0]).startswith("secondary_name"):
						nnames+=1
						names.append({})
						names[nnames]['secondary_name']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("atom_uid"):
						natoms+=1
						atoms.append({})
						atoms[natoms]['atom_uid']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("atom_nb"):
						atoms[natoms]['atom_nb']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("link_name"):
						nlinks+=1
						links.append({})
						links[nlinks]['link_name']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("link_url"):
						links[nlinks]['link_url']=str(l.split("\t")[1])
					else:
						if str(l.split("\t")[0])!='':
							csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")

	#Checking the validity of the input file
	check_lines_th = 19 + 1*(nnames+1) + 2*(natoms+1) + 2*(nlinks+1)
	check_lines_eff=len(csvdata)-1
	for nname in range(len(names)):
		check_lines_eff=check_lines_eff+len(names[nname])
	for natom in range(len(atoms)):
		check_lines_eff=check_lines_eff+len(atoms[natom])
	for nlink in range(len(links)):
		check_lines_eff=check_lines_eff+len(links[nlink])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="molecule-ion_"+csvdata['uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Check what you want to do!!!")
					correctanswer=True
					samename=False
					exitprog=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True
	if exitprog:
		sys.exit("This user already exists. Check what you want to do!!!")

	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('molecule'):
		detail.find('uid').text='MOLION_'+csvdata['uid']
		detail.find('import_mode').text=csvdata['import_mode']
		detail.find('name').text=csvdata['name']
		detail.find('iupac_name').text=csvdata['iupac_name']
		detail.find('inchi').text=csvdata['inchi']
		detail.find('inchikey').text=csvdata['inchikey']
		detail.find('cas_number').text=csvdata['cas_number']
		detail.find('formula').text='<![CDATA['+csvdata['formula']+']]>'
		detail.find('chemical_formula').text=csvdata['chemical_formula']
		detail.find('chemical_structure_filename').text=csvdata['chemical_structure_filename']
		detail.find('structural_formula').text='<![CDATA['+csvdata['structural_formula']+']]>'
		detail.find('charge').text=csvdata['charge']
		detail.find('unpaired_electrons').text=csvdata['unpaired_electrons']
		detail.find('symmetry').text=csvdata['symmetry']
		detail.find('case').text=csvdata['case']
		detail.find('molar_mass').text=csvdata['molar_mass']
		detail.find('protic').text=csvdata['protic']
		detail.find('polarity').text=csvdata['polarity']
		detail.find('comments').text='<![CDATA['+csvdata['comments']+']]>'

	#Rewriting the xml (elements of the levels 1 and more)
	for item in root.getchildren():
		level1=item.getchildren()
		for n1 in range(len(level1)):
			if level1[n1].tag=='secondary_names':
				for nname in range(nnames):
					#adding as many blocks as nnames
					level1[n1].append( deepcopy(level1[n1][0]) )
				nname=0
				for elem in level1[n1].findall("secondary_name"):
					elem.text=names[nname]['secondary_name']
					nname+=1
			if level1[n1].tag=='atoms':
				for natom in range(natoms):
					#adding as many blocks as natoms
					level1[n1].append( deepcopy(level1[n1][1]) )
				natom=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							level2[n2].text=atoms[natom]['atom_uid']
						if level2[n2].tag=='number':
							level2[n2].text=atoms[natom]['atom_nb']
							natom+=1
			if level1[n1].tag=='links':
				for nlink in range(nlinks):
					#adding as many blocks as nlinks
					level1[n1].append( deepcopy(level1[n1][1]) )
				nlink=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='name':
							level2[n2].text='<![CDATA['+links[nlink]['link_name']+']]>'
						if level2[n2].tag=='url':
							level2[n2].text='<![CDATA['+links[nlink]['link_url']+']]>'
							nlink+=1

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="species" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="species" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                            SOLID                                                     #
#======================================================================================================================#
def solid_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for solid files.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''

	xmlfile = templatedir +'/'+ "solid.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)
	
	#Reading the csv file and storing the data
	csvdata={}
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				names=[]
				nnames=-1
				species=[]
				nspecies=-1
				links=[]
				nlinks=-1
				figs=[]
				nfigs=-1
				for l in f:
					if str(l.split("\t")[0]).startswith("secondary_name"):
						nnames+=1
						names.append({})
						names[nnames]['secondary_name']=str(l.split("\t")[1])

					elif str(l.split("\t")[0]).startswith("specie_uid"):
						nspecies+=1
						species.append({})
						species[nspecies]['specie_uid']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("specie_nb_min"):
						species[nspecies]['specie_nb_min']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("specie_nb_max"):
						species[nspecies]['specie_nb_max']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("state"):
						species[nspecies]['state']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("relevance"):
						species[nspecies]['relevance']=str(l.split("\t")[1])

					elif str(l.split("\t")[0]).startswith("link_name"):
						nlinks+=1
						links.append({})
						links[nlinks]['link_name']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("link_url"):
						links[nlinks]['link_url']=str(l.split("\t")[1])
					
					elif str(l.split("\t")[0]).startswith("figure_filename"):
						nfigs+=1
						figs.append({})
						figs[nfigs]['figure_filename']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("figure_caption"):
						figs[nfigs]['figure_caption']=str(l.split("\t")[1])

					else:
						if str(l.split("\t")[0])!='':
							csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")
	
	#Checking the validity of the input file
	check_lines_th = 26 + 1*(nnames+1) + 5*(nspecies+1) + 2*(nlinks+1) + 2*(nfigs+1)
	check_lines_eff=len(csvdata)-1
	for nname in range(len(names)):
		check_lines_eff=check_lines_eff+len(names[nname])
	for nspecie in range(len(species)):
		check_lines_eff=check_lines_eff+len(species[nspecie])
	for nlink in range(len(links)):
		check_lines_eff=check_lines_eff+len(links[nlink])
	for nfig in range(len(figs)):
		check_lines_eff=check_lines_eff+len(figs[nfig])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="solid_"+csvdata['uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Check what you want to do!!!")
					correctanswer=True
					samename=False
					exitprog=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True
	if exitprog:
		sys.exit("This user already exists. Check what you want to do!!!")
	
	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('solid'):
		detail.find('uid').text='SOLID_'+csvdata['uid']
		detail.find('import_mode').text=csvdata['import_mode']
		detail.find('official_name').text=csvdata['official_name']
		detail.find('mineral_uid').text=csvdata['mineral_uid']
		detail.find('cas_number').text=csvdata['cas_number']
		detail.find('family').text=csvdata['family']
		detail.find('class').text=csvdata['class']
		detail.find('compound_type').text=csvdata['compound_type']
		detail.find('classification_level').text=csvdata['classification_level']
		detail.find('formula').text='<![CDATA['+csvdata['formula']+']]>'
		detail.find('chemical_formula').text=csvdata['chemical_formula']
		detail.find('hydration').text=csvdata['hydration']
		detail.find('hydration_series').text=csvdata['hydration_series']
		detail.find('hydration_number').text=csvdata['hydration_number']
		detail.find('classification_class').text=csvdata['classification_class']
		detail.find('classification_type').text=csvdata['classification_type']
		#detail.find('classification_group').text=csvdata['classification_group']
		#detail.find('classification_code').text=csvdata['classification_code']
        #Forced to NULL in the template by Bernard
		detail.find('phase_name').text=csvdata['phase_name']
		detail.find('phase_type').text=csvdata['phase_type']
		detail.find('crystal_system').text=csvdata['crystal_system']
		detail.find('crystal_class').text=csvdata['crystal_class']
		detail.find('crystal_class_symbol').text=csvdata['crystal_class_symbol']
		detail.find('crystal_spacegroup').text=csvdata['crystal_spacegroup']
		detail.find('molar_mass').text=csvdata['molar_mass']
		detail.find('density').text=csvdata['density']
		detail.find('pure_color').text=csvdata['pure_color']
		detail.find('comments').text='<![CDATA['+csvdata['comments']+']]>'
	
	#Rewriting the xml (elements of the levels 1 and more)
	for item in root.getchildren():
		level1=item.getchildren()
		for n1 in range(len(level1)):
			if level1[n1].tag=='secondary_names':
				for nname in range(nnames):
					#adding as many blocks as nnames
					level1[n1].append( deepcopy(level1[n1][0]) )
				nname=0
				for elem in level1[n1].findall("secondary_name"):
					elem.text=names[nname]['secondary_name']
					nname+=1
			if level1[n1].tag=='species':
				for nspecie in range(nspecies):
					#adding as many blocks as nspecies
					level1[n1].append( deepcopy(level1[n1][1]) )
				nspecie=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							level2[n2].text=species[nspecie]['specie_uid']
						if level2[n2].tag=='number_min':
							level2[n2].text=species[nspecie]['specie_nb_min']
						if level2[n2].tag=='number_max':
							level2[n2].text=species[nspecie]['specie_nb_max']
						if level2[n2].tag=='state':
							level2[n2].text=species[nspecie]['state']
						if level2[n2].tag=='relevance':
							level2[n2].text=species[nspecie]['relevance']
							nspecie+=1
			if level1[n1].tag=='links':
				for nlink in range(nlinks):
					#adding as many blocks as nlinks
					level1[n1].append( deepcopy(level1[n1][1]) )
				nlink=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='name':
							level2[n2].text='<![CDATA['+links[nlink]['link_name']+']]>'
						if level2[n2].tag=='url':
							level2[n2].text='<![CDATA['+links[nlink]['link_url']+']]>'
							nlink+=1
			if level1[n1].tag=='figures':
				for nfig in range(nfigs):
					#adding as many blocks as nfigs
					level1[n1].append( deepcopy(level1[n1][1]) )
				nfig=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='filename':
							level2[n2].text=figs[nfig]['figure_filename']
						if level2[n2].tag=='caption':
							level2[n2].text='<![CDATA['+figs[nfig]['figure_caption']+']]>'
							nfig+=1
	
	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="fundamental_phase" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="fundamental_phase" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")

#======================================================================================================================#
#                                                           MINERAL                                                    #
#======================================================================================================================#
def miner_file_writer(templatedir,expdir,csvfilename):
	'''
	csv to xml converter for mineral files.
	--> use the excel file and save the corresponding page in csv (UTF-8 / Tabulation separators) or
		use the excel file and the ods_converter function.
	--> give 3 arguments: template path, data directory and csv file
	'''

	xmlfile = templatedir +'/'+ "mineral.xml"
	xml = open(xmlfile,'r')
	tree = etree.parse(xml)
	root = tree.getroot()

	tmpfilename = 'tmp.xml'

	os.chdir(expdir)
	
	#Reading the csv file and storing the data
	csvdata={}
	csvfile = expdir +'/'+ csvfilename
	if csvfilename.endswith('.csv'):
		try:
			with open(csvfile,'r') as f:
				species=[]
				nspecies=-1
				links=[]
				nlinks=-1
				phs=[]
				nphs=-1
				for l in f:
					if str(l.split("\t")[0]).startswith("specie_uid"):
						nspecies+=1
						species.append({})
						species[nspecies]['specie_uid']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("specie_nb_min"):
						species[nspecies]['specie_nb_min']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("specie_nb_max"):
						species[nspecies]['specie_nb_max']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("state"):
						species[nspecies]['state']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("relevance"):
						species[nspecies]['relevance']=str(l.split("\t")[1])

					elif str(l.split("\t")[0]).startswith("link_name"):
						nlinks+=1
						links.append({})
						links[nlinks]['link_name']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("link_url"):
						links[nlinks]['link_url']=str(l.split("\t")[1])
					
					elif str(l.split("\t")[0]).startswith("phase_transition_type"):
						nphs+=1
						phs.append({})
						phs[nphs]['phase_transition_type']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("other_phases"):
						phs[nphs]['other_phases']=str(l.split("\t")[1])
					elif str(l.split("\t")[0]).startswith("temperature"):
						phs[nphs]['temperature']=str(l.split("\t")[1])

					else:
						if str(l.split("\t")[0])!='':
							csvdata[str(l.split("\t")[0])] = str(l.split("\t")[1])
		except:
			sys.exit("File "+csvfile+" doesn't exist!")
	else:
		sys.exit("Your file should be a .csv file!!!")
	
	#Checking the validity of the input file
	check_lines_th = 41 + 5*(nspecies+1) + 2*(nlinks+1) + 3*(nphs+1)
	check_lines_eff=len(csvdata)-1
	for nspecie in range(len(species)):
		check_lines_eff=check_lines_eff+len(species[nspecie])
	for nlink in range(len(links)):
		check_lines_eff=check_lines_eff+len(links[nlink])
	for nph in range(len(phs)):
		check_lines_eff=check_lines_eff+len(phs[nph])
	if check_lines_th-check_lines_eff:
		sys.exit("File "+csvfile+" has "+str(check_lines_eff)+" parametre lines instead of the "+str(check_lines_th)+" expected!!!")

	#Checking the validity of the ouput file name
	valid=False
	exitprog=False
	while not valid:
		outputfilename="miner_"+csvdata['uid']+".xml"
		ls=os.listdir()
		samename=outputfilename in ls
		if samename:
			print("FILE "+outputfilename+" ALREADY EXISTS")
			correctanswer=False
			while not correctanswer:
				OW=input("Do you want to overwrite it? ").lower()
				if OW=="yes" or OW=="y":
					correctanswer=True
					samename=False
				elif OW=="no" or OW=="n":
					print("Check what you want to do!!!")
					correctanswer=True
					samename=False
					exitprog=True
				else:
					print("!!! INVALID ANSWER !!!")
					print("Please type Y or N ")
		if not samename:
			valid=True
	if exitprog:
		sys.exit("This user already exists. Check what you want to do!!!")
	
	#Rewriting the xml (elements of the level 0)
	for detail in root.findall('mineral'):
		detail.find('uid').text='MINER_'+csvdata['uid']
		detail.find('import_mode').text=csvdata['import_mode']
		detail.find('ima_name').text=csvdata['ima_name']
		detail.find('secondary_name').text=csvdata['secondary_name']
		detail.find('cas_number').text=csvdata['cas_number']
		detail.find('class').text=csvdata['class']
		detail.find('compound_type').text=csvdata['compound_type']
		detail.find('classification_level').text=csvdata['classification_level']
		detail.find('formula').text='<![CDATA['+csvdata['formula']+']]>'
		detail.find('chemical_formula').text=csvdata['chemical_formula']
		detail.find('hydration').text=csvdata['hydration']
		detail.find('hydration_series').text=csvdata['hydration_series']
		detail.find('hydration_number').text=csvdata['hydration_number']
		detail.find('strunz_class').text=csvdata['strunz_class']
		detail.find('strunz_division').text=csvdata['strunz_division']
		detail.find('strunz_family').text=csvdata['strunz_family']
		detail.find('strunz_code').text=csvdata['strunz_code']
		detail.find('dana_major_class').text=csvdata['dana_major_class']
		detail.find('dana_class').text=csvdata['dana_class']
		detail.find('dana_type').text=csvdata['dana_type']
		detail.find('dana_group').text=csvdata['dana_group']
		detail.find('dana_code').text=csvdata['dana_code']
		detail.find('phase_type').text=csvdata['phase_type']
		detail.find('crystal_system').text=csvdata['crystal_system']
		detail.find('crystal_class').text=csvdata['crystal_class']
		detail.find('crystal_class_symbol').text=csvdata['crystal_class_symbol']
		detail.find('crystal_spacegroup').text=csvdata['crystal_spacegroup']
		detail.find('molar_mass').text=csvdata['molar_mass']
		detail.find('density').text=csvdata['density']
		detail.find('refringence_type').text=csvdata['refringence_type']
		detail.find('refringence_sign').text=csvdata['refringence_sign']
		detail.find('birefringence').text=csvdata['birefringence']
		detail.find('refraction_index_na').text=csvdata['refraction_index_na']
		detail.find('refraction_index_nb').text=csvdata['refraction_index_nb']
		detail.find('refraction_index_ng').text=csvdata['refraction_index_ng']
		detail.find('dispersion_v').text=csvdata['dispersion_v']
		detail.find('pure_color').text=csvdata['pure_color']
		detail.find('true_color').text=csvdata['true_color']
		detail.find('diaphaneity').text=csvdata['diaphaneity']
		detail.find('luster').text=csvdata['luster']
		detail.find('comments').text='<![CDATA['+csvdata['comments']+']]>'
	
	#Rewriting the xml (elements of the levels 1 and more)
	for item in root.getchildren():
		level1=item.getchildren()
		for n1 in range(len(level1)):
			if level1[n1].tag=='species':
				for nspecie in range(nspecies):
					#adding as many blocks as nspecies
					level1[n1].append( deepcopy(level1[n1][0]) )
				nspecie=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='uid':
							level2[n2].text=species[nspecie]['specie_uid']
						if level2[n2].tag=='number_min':
							level2[n2].text=species[nspecie]['specie_nb_min']
						if level2[n2].tag=='number_max':
							level2[n2].text=species[nspecie]['specie_nb_max']
						if level2[n2].tag=='state':
							level2[n2].text=species[nspecie]['state']
						if level2[n2].tag=='relevance':
							level2[n2].text=species[nspecie]['relevance']
							nspecie+=1
			if level1[n1].tag=='links':
				for nlink in range(nlinks):
					#adding as many blocks as nlinks
					level1[n1].append( deepcopy(level1[n1][0]) )
				nlink=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='name':
							level2[n2].text='<![CDATA['+links[nlink]['link_name']+']]>'
						if level2[n2].tag=='url':
							level2[n2].text='<![CDATA['+links[nlink]['link_url']+']]>'
							nlink+=1
			if level1[n1].tag=='phase_transitions':
				for nph in range(nphs):
					#adding as many blocks as nphs
					level1[n1].append( deepcopy(level1[n1][0]) )
				nph=0
				for item in level1[n1].getchildren():
					level2=item.getchildren()
					for n2 in range(len(level2)):
						if level2[n2].tag=='type':
							level2[n2].text=phs[nph]['phase_transition_type']
						if level2[n2].tag=='other_phases':
							level2[n2].text=phs[nph]['other_phases']
						if level2[n2].tag=='temperature':
							level2[n2].text=phs[nph]['temperature']
							nph+=1

	#Writing the output file
	tree.write(tmpfilename,encoding='UTF-8')

	#To write properly the header and solve the <> characters problem in the comments:
	outputfile = open(outputfilename,'w')
	outputfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
	with open(tmpfilename,'r') as f:
		for l in f:
			if '<import type="experimentalist" ssdm_version="0.9.0">' in l:
				outputfile.write('<import type="experimentalist" ssdm_version="0.9.0"\n')
				outputfile.write('\txmlns="http://sshade.eu/schema/import"\n')
				outputfile.write('\txmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
				outputfile.write('\txsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">\n')
			elif "&lt;" in l:
				lend=l.split('&lt;')[1]
				outputfile.write(l.split('&lt;')[0]+'<'+lend.split('&gt;')[0]+'>'+lend.split('&gt;')[1])
			else:
				outputfile.write(l)
	print("\n=== The file "+outputfilename+" has been written in "+os.getcwd()+". ===")
	

#======================================================================================================================#
#                                                    MULTIPLE FILE WRITER                                              #
#======================================================================================================================#
def multiple_file_writer(templatedir,expdir,writer_type):
	'''
	function calling one of the converters to process all the appropriate data in a choosen directory.
	give 3 arguments: the template path, the data directory and the type of files you want to process:
	experimentalist, laboratory, pellet, frozen_solution, HPHT_solution, simple_experiment (=experiments_spectra),
	multiple_experiment (=experiments_spectra_with_structure), molion, solid.
	'''

	writer_types = ["experimentalist", "laboratory", "pellet", "frozen_solution", "HPHT_solution",\
					"simple_experiment", "multiple_experiment", "molec", "molion", "solid", "miner"]

	if writer_type not in writer_types:
		sys.exit("Possible writer types: experimentalist, laboratory, pellet, frozen_solution,\
			HPHT_solution, simple_experiment, multiple_experiment, molec, molion, solid, miner.")

	os.chdir(expdir)
	nofile = 1
	for f in os.listdir():
		if writer_type == "experimentalist":
			if f.startswith("experimentalist") and f.endswith(".csv"):
				experimentalist_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "laboratory":
			if f.startswith("laboratory") and f.endswith(".csv"):
				laboratory_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "pellet":
			if f.startswith("pellet") and f.endswith(".csv"):
				sample_pellet_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "frozen_solution":
			if f.startswith("frozen_solution") and f.endswith(".csv"):
				sample_frozen_solution_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "HPHT_solution":
			if f.startswith("HPHT_solution") and f.endswith(".csv"):
				sample_HPHT_solution_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "simple_experiment":
			if f.startswith("simple_experiment") and f.endswith(".csv"):
				experiments_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "multiple_experiment":
			if f.startswith("multiple_experiment") and f.endswith(".csv"):
				experiments_structure_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "molec":
			if f.startswith("molec") and f.endswith(".csv"):
				molec_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "molion":
			if f.startswith("molion") and f.endswith(".csv"):
				molion_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "solid":
			if f.startswith("solid") and f.endswith(".csv"):
				solid_file_writer(templatedir,expdir,f)
				nofile = 0
		if writer_type == "miner":
			if f.startswith("miner") and f.endswith(".csv"):
				miner_file_writer(templatedir,expdir,f)
				nofile = 0

	if nofile:
		print("No "+writer_type+" files to process in "+os.getcwd()+".")
