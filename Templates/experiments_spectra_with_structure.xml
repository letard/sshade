<import type="experiment_spectra" ssdm_version="0.9.0">

	<experiment>
		<import_mode>$$$</import_mode> <!-- experiment import_mode -->
		<uid>$$$</uid> <!-- experiment uid - Ex: EXPERIMENT_OP_20180115_005 -->

		<owner_databases>
			<database_uid>DB_FAME</database_uid>
		</owner_databases>
		<types>
			<type>laboratory measurement</type>
		</types>

		<title><![CDATA[]]></title> <!-- experiment title - Fe K edge XAS fluorescence of iron ferrous solution in hydrothermal conditions at 500bars and between 25 and 450°C -->
		<description><![CDATA[]]></description> <!-- Chloride molality varying from 0.5 and 12 m; temperature varying from 25 to 450 °C -->
		<date_begin>$$$</date_begin> <!-- Ex: 2015-09-03 -->
		<date_end></date_end>

		<laboratory_uid>LAB_FAME</laboratory_uid>

		<experimentalists> <!-- Ex: EXPER_Olivier_Proux -->
			<experimentalist_uid>$$$</experimentalist_uid>
		</experimentalists>
		
		<variable_parameters_types> <!-- Should be two -->
			<variable_parameters_type>$$$</variable_parameters_type>
			<variable_parameters_type>$$$</variable_parameters_type>
			<!-- multiple --> <!-- **MANDATORY** Type of sample, environment, instrument or spectrum variable
			parameters of the experiment. OpenEnum: {no, sample composition, sample abundance, sample size,
			sample thickness, sample texture, sample grain size, sample phase, constituent, chemical variability,
			temperature, pressure, mechanical stress, reactant, time, irradiation type, irradiation energy,
			irradiation dose, spectrum type, spectral range, illumination-observation geometry, incidence angle,
			emergence angle, azimuth angle, phase angle, polarization, observation mode, other} -->
		</variable_parameters_types>

		<parameters_instruments>
			<parameters_instrument>
				<instrument_uid>INSTRU_XAS_FLUO_FAME</instrument_uid><!-- Ex: To be found in the database -->
				<instrument_sample_holder><![CDATA[]]></instrument_sample_holder>
				<spectral>
					<unit>eV</unit>
					<standard>unknown</standard>
					<observation_mode>spectrum</observation_mode>
					<range_types>
						<type>hard X</type>
					</range_types> 
					<ranges>
						<range>					
							<min>$$$</min>
							<max>$$$</max>
							<absorption_edge_element_uid>ATOM_$$$</absorption_edge_element_uid>
							<absorption_edge_type>K</absorption_edge_type>
                                                        <!-- Enum: {K, L, L1, L2, L3, L2-3, M, M1, M2, M3, M4, M5, M4-5, N,
                                                        N1, N2, N3, N4, N5, N6, N7, O, O1, O2, O3, O4, O5, O6, O7} -->
                                                        <resolution>$$$</resolution> <!-- monochromator resolution -->
						</range>
					</ranges>
					<filters>
						<filter>
							<type>NULL</type>
							<center>NULL</center>
							<width>NULL</width>
						</filter>
					</filters>
					<comments><![CDATA[The resolution in the above table is the resolution given by the monochromator. The energy was calibrated by regularly measuring the spectrum of a reference metallic foil (first maximum of the derivative set at 7112 eV).]]></comments>
				</spectral>
				<angle>
					<observation_geometry>$$$</observation_geometry>  <!-- Enum: {direct (if transmission), directional (if fluorescence emission} -->
					<observation_mode>fixed angles</observation_mode>
					<incidence>NULL</incidence>
					<emergence>NULL</emergence>
					<azimuth>NULL</azimuth>
					<resolution_observation>NULL</resolution_observation>
					<comments></comments>
				</angle>
				<spatial> 
					<observation_mode>$$$</observation_mode> <!-- Enum: {single spot, averaged} -->
					<spots_number>NULL</spots_number>
				</spatial>
			</parameters_instrument>
		</parameters_instruments>

		<comments><![CDATA[These data were acquired on $$$ in the frame of the $$$ proposal.]]></comments>

		<publications>
			<publication_uid>PUBLI_$$$</publication_uid>
		</publications>
		
		<structure>
			<sections variable_parameter="$$$"> <!-- same as line 33 -->
				<section>
					<title><![CDATA[Iron ferrous solution in hydrothermal conditions: Fe2+ 0.045m, HCl 0.1m, Na 0.356m, Cl 0.547m, 500bar [25-450°C]]]></title>
					<spectra variable_parameter="$$$"> <!-- same as line 34 -->
						<spectrum_uid>$$$</spectrum_uid>
					</spectra>				
				</section>
			</sections>
		</structure>
		
		<spectra>
			<spectrum> <!-- A multiplier -->
				<import_mode>$$$</import_mode> <!-- spectrum import_mode. Enum: {first import, inherited, ignore, draft, no change, correction, new version, invalidate} -->
				<uid>$$$</uid>  <!-- Ex: SPECTRUM_OP_20180115_005T -->
				<experiment_type>laboratory measurement</experiment_type>
				<chronologically_ordered>no</chronologically_ordered>
				<title><![CDATA[]]></title> <!-- Fe K edge XAS fluorescence of iron ferrous solution (Fe2+ 0.045m, HCl 0.1m, Na 0.356m, Cl 0.547m) at 500bar and 25°C-->
				<type>$$$</type> <!-- spectrum type -->
				<!-- Enum: {fluorescence emission, transmission, raw, absorbance, normalized absorbance, optical density,
				absorption coefficient, optical constants, ATR transmission, ATR absorbance, corrected ATR absorbance, admittance,
				impedance, dielectric constant, electric conductivity, magnetic permeability, electric permittivity, loss tangent,
				reflectance, specular reflectance, complex reflectance ratio, bidirectional reflectance, bidirectional
				reflectance factor, BRDF, MBRDF, SBRDF, hemispheric-directional reflectance, directional-hemispheric albedo,
				thermal emission, thermal radiance, thermal emittance, directional thermal emissivity, DEDF, MDEDF, SDEDF,
				hemispheric thermal emissivity, scattering, radiative transfer model parameters, Raman scattering,
				normalized Raman scattering, Raman scattering coefficient, Raman scattering efficiency, fluorescence emission,
				normalized fluorescence emission, fluorescence emission efficiency} -->
				<intensity_unit>AU</intensity_unit>
				<sample>
					<uid>$$$</uid> <!-- sample uid -->	
				</sample>					

				<parameters_environment>
					<time_unit>min</time_unit>
					<temperature>
						<unit>K</unit> <!-- Enum: {K, C, F} -->
						<value>$$$</value>
						<error>$$$</error>
						<time>$$$</time>
					</temperature>

					<pressure>
						<unit>bar</unit> <!-- Enum: {Pa, hPa, MPa, GPa, mbar, bar, atm, torr} -->
						<value>$$$</value>
						<error>$$$</error>
						<time>$$$</time> <!-- same as line 113 -->
					</pressure>
					<fluid>
						<type>$$$</type> <!-- fluid type - Put nothing or Enum: {atomic gas, plasma, molecular gas, molecular liquid, liquid solution, ambient air, purged air, vacuum, other} -->
						<temperature>$$$</temperature> <!-- fluid temperature -->
						<pressure_unit>bar</pressure_unit>
						<pressure>$$$</pressure>
						<time>$$$</time> <!-- same as line 113 -->
						<composition_species>
							<composition_specie>
								<uid>$$$</uid> <!-- fluid composition specie uid - Put nothing or ATOM_He -->
								<mole_fraction>$$$</mole_fraction> <!-- Nothing or 1 -->
								<mole_fraction_error></mole_fraction_error>
							</composition_specie>
						</composition_species>
					</fluid>
				</parameters_environment>
				
				<parameters_instruments>
					<parameters_instrument>
						<instrument_uid>INSTRU_XAS_$$$</instrument_uid> <!-- One of those listed at the beginning of the experiment file -->
						<spectral>
							<comments><![CDATA[$$$]]></comments>	<!-- Additional information on spectral parameters: special range type, narrow-band filter peak transmission and transmission shape, info on variable spectral resolution ... [blob] -->
						</spectral>
					</parameters_instrument>
				</parameters_instruments>
				
				<date_begin>$$$</date_begin> <!-- Ex: 2015-09-03 -->
				
				<analysis><![CDATA[raw averaged spectra]]></analysis>
				<quality_flag>NULL</quality_flag> <!-- Between 1 and 5 -->
				<publications>
					<publication_uid>PUBLI_$$$</publication_uid>
				</publications>
										
				<files>
					<parameter>
						<type>single spectrum</type>
						<format>ascii-columns</format>
						<header_lines_number>$$$</header_lines_number>
						<column_total_number>$$$</column_total_number>
						<column_separator>$$$</column_separator>
						<columns>
							<column>
								<number>$$$</number>
								<type>position</type>
							</column>
							<column>
								<number>$$$</number>
								<type>intensity</type>
							</column>
						</columns>	
					</parameter>
					<file>
						<filename>$$$.xmu</filename>
					</file>
				</files>
				<export_filename>XAS_fluo_FerrousSolutionA_500b_025deg</export_filename>
				
				<experiment_preview>
					<flag>yes</flag>
				</experiment_preview>
			</spectrum>
		</spectra>
	</experiment>
</import>
