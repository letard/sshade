<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Data type : Species

Specific notes :
	- molecules are entered in the database either as pure isotope, terrestrial mixture of isotopes (natural atom) or in some cases partly substituted molecules.
	- <isotope_molecules> bloc is only used for partly substituted bonds and terrestrial mmixture of isotopes
	
General notes : 
	- Most of the tags are optional, you can remove the really unnecessary ones.
	- Tags marked as 'multiple' can be copied (with its block of sub-tag, up to the ending tag) if needed.
	- all blocs marked "OPTION" can be fully removed if not needed (now or in the future)
	- **ABS MANDATORY / ABS COMPULSORY**: a value need to be absolutely provided, no way to escape! (SSHADE will not function properly if absent).
	- **MANDATORY / COMPULSORY**: very important values for the search of the data. If the value (txt or numeric) of one tag is not known (or irrelevant in your case), then put 'NULL' and write a comment to keep track of the missing value. Remove comment when value is added.
	- **MANDATORY / COMPULSORY only for ...**: when a value is optionally MANDATORY the condition is written. 
	- "LINK to existing UID" (unique identifier): references to another table in SSHADE. You have to reconstruct (easy for some: rule is in comment) or found this existing UID in the database beforehand (use "Provider/Full Search" menu in SSHADE).
	- "UID to CREATE": you need to create this UID using their specific rules of creation that are explained in their attached comment. Use only alphanumeric characters and '_'.
	- Enumeration type ("Enum" or "OpenEnum") must contain one single item from the list given in brackets {}.
	- use a CDATA tag when a value contains at least one special character (ie: &, >, <,...). Example: <![CDATA[AT&T]]> for AT&T
	- The data format is noted beetween [] and is 'ascii' when not specified. Ex: [Float], [Integer], ...  
	- when no numerical format or Enum is specified, it is free text but limited to 256 characters. Only those noted [blob] have no size limitation.
	- to import data for the first time you have to set <import_mode>='first import'. To correct data you have to change it to 'correction'.
	- when a <filename> is given, then the file should be ziped with this xml file for import.  
-->
<import type="species" ssdm_version="0.9.0"
        xmlns="http://sshade.eu/schema/import"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">

<!-- MOLECULES, MOLECULAR IONS, FREE RADICALS -->
	<molecule><!-- multiple -->
		<import_mode>first import</import_mode> <!-- **ABS MANDATORY** Mode of import of the 'species' data. Enum: {first import, ignore, draft, no change, correction} -->
		<uid>MOLION_BrO3_-</uid> <!-- **ABS MANDATORY to CREATE** Unique identifier code given to the molecular species. Should be of the style ‘MOLEC_(Isotopic)ChemicalFormula(Letter)’, 'MOLION_', 'MOLRAD_', 'MOLRADION_' or 'MOLMER_': ‘MOLEC_13C16O18O’, molecular ion: ‘MOLION_CO2+’, or radical: ‘MOLRAD_OH’ -->
		
	<!-- MOLECULE TYPE and NAMES -->
		<type>molecular ion</type> <!-- **ABS MANDATORY** Type of molecular species. Enum: {molecule, molecular ion, molecular radical, molecular ionic radical, molecular mer} -->	
		<name>Bromate ion</name> <!-- **ABS MANDATORY** Generic name of the molecule, independent on isotopic species -->
		<secondary_names> <!-- **OPTION** -->
			<secondary_name>Bromate anion</secondary_name><!-- multiple --> <!-- Secondary name of the molecule, may depend on isotopic species -->
			<secondary_name>Bromate(1-) ion</secondary_name>
			<secondary_name>Trioxidobromate(1-)</secondary_name>
			<secondary_name>Bromic acid, ion(1-)</secondary_name>
			<secondary_name>BrO3-</secondary_name>
			<secondary_name>BrO3(-)</secondary_name>
		</secondary_names> 
		<iupac_name>Bromate</iupac_name> <!-- **ABS MANDATORY** IUPAC name of the molecule (depend on isotopic species, ex: (1H2,16O)Water) -->
		
	<!-- MOLECULE IDENTIFIERS -->
		<inchi>1S/BrHO3/c2-1(3)4/h(H,2,3,4)/p-1</inchi> <!-- **MANDATORY only for molecules** Standard InChI sequence, including isotopes -->
		<inchikey>SXDBWCPKPHAZSM-UHFFFAOYSA-M</inchikey> <!-- **MANDATORY only for molecules** Standard InChI sequence coded over 25 characters -->
		<cas_number>15541-45-4</cas_number> <!-- CAS registry number ... if available on the web !! -->
		
	<!-- MOLECULE CHEMICAL STRUCTURE AND ATOMIC COMPOSITION	-->
		<formula><![CDATA[$BrO_3^-$]]></formula> <!-- **ABS MANDATORY** Developed chemical formula of the molecule, molecular ion or radical. Contain isotopic, radical, conformational and ionic information [LaTeX format] -->
		<chemical_formula>(BrO3)-</chemical_formula> <!-- **ABS MANDATORY** Simple chemical formula of the molecule, molecular ion, or free radical. No isotopic, radical and conformational information -->
		<chemical_structure_filename>Bromate-ion.png</chemical_structure_filename> <!-- **MANDATORY** Image file name with 2D or 3D representation of the chemical and isotopic structure of the molecule (.jpg, .png, .gif ...). This file should be zipped with the xml file -->
		<structural_formula><![CDATA[[[O-]Br(=O)=O]]]></structural_formula> <!-- **MANDATORY** Semi-developped SMILES structural chemical formula of the molecule: natural or isotopes, charge, conformation ... -->
		<chemical_functions> <!-- **MANDATORY** --> <!-- list of the different chemical functions (functional groups) of the molecular species -->
			<chemical_function><!-- multiple -->
				<uid>NULL</uid> <!-- **MANDATORY** LINK to the existing UID of the 'functional group’ of the molecular species [‘GROUPMOLEC_RGroupFormula’, ‘GROUPION__RGroupFormula(Charge)’, ‘GROUPRAD__RGroupFormula(Charge)’, or ‘MINION_AOn_Charge'] -->				
				<number>NULL</number> <!-- **MANDATORY** Number of functional groups of each type composing the molecular species -->
			</chemical_function>
		</chemical_functions>
		<chemical_bonds> <!-- **MANDATORY** List of the different chemical bonds of the molecular species -->
			<chemical_bond><!-- multiple -->
				<uid>BOND_ddBrO-_5</uid> <!-- **MANDATORY** LINK to the existing UID of the 'chemical bonds' of the molecular species [‘BOND_ZAtom1(BondSymbol)ZAtom2(Charge)’ or ‘MOLECPART_’] -->	
				<number>1</number> <!-- **MANDATORY** Number of bonds of each type composing the molecular species -->
			</chemical_bond>
			<chemical_bond><!-- multiple -->
				<uid>BOND_dBrdO_5</uid> <!-- **MANDATORY** LINK to the existing UID of the 'chemical bonds' of the molecular species [‘BOND_ZAtom1(BondSymbol)ZAtom2(Charge)’ or ‘MOLECPART_’] -->	
				<number>2</number> <!-- **MANDATORY** Number of bonds of each type composing the molecular species -->
			</chemical_bond>
		</chemical_bonds>
		<atoms> <!-- **ABS MANDATORY** List of the different atoms of the molecular species and their number -->
			<atom><!-- multiple -->
				<uid>ATOM_Br</uid> <!-- **ABS MANDATORY** LINK to the existing UID of the different atoms (isotopic or natural species) composing the molecule, if known [‘ATOM_(Z)Symbol’ or 'ATION_(Z)SymbolCharge'] -->
				<number>1</number> <!-- **MANDATORY** Total number of this atom (natural mixture or specific isotope) in the molecule [integer] -->
			</atom>
			<atom><!-- multiple -->
				<uid>ATOM_O</uid> <!-- **ABS MANDATORY** LINK to the existing UID of the different atoms (isotopic or natural species) composing the molecule, if known [‘ATOM_(Z)Symbol’ or 'ATION_(Z)SymbolCharge'] -->
				<number>2</number> <!-- **MANDATORY** Total number of this atom (natural mixture or specific isotope) in the molecule [integer] -->
			</atom>
			<atom><!-- multiple -->
				<uid>ATION_O-</uid> <!-- **ABS MANDATORY** LINK to the existing UID of the different atoms (isotopic or natural species) composing the molecule, if known [‘ATOM_(Z)Symbol’ or 'ATION_(Z)SymbolCharge'] -->
				<number>1</number> <!-- **MANDATORY** Total number of this atom (natural mixture or specific isotope) in the molecule [integer] -->
			</atom>
		</atoms>
		<charge>-1</charge> <!-- **ABS MANDATORY** Charge (positive or negative) of the molecular species [signed integer] -->	
		<unpaired_electrons>0</unpaired_electrons> <!-- **MANDATORY** Number (positive) of unpaired electrons free [integer] -->

	<!-- MOLECULE: STEREO-ISOMERS -->
		<stereoisomer>
			<stereodescriptor></stereodescriptor> <!-- **MANDATORY and only for stereoisomer** Type of stereoisomer of the molecular species (CIP rules) FreeList: {R, S, E, Z, P, M, cis, trans, alpha, beta, syn, anti, clinal, periplanar, synperiplanar, synclinal, anticlinal, antiperiplanar, and any combination: RS, SZ, ..…} -->
			<relations>
				<relation>no</relation> <!-- **MANDATORY** Relation of stereo-isomery existing with the molecular species. Enum: {enantiomer, diastereomer, anomer, atropisomer, conformer, other, no} -->
			</relations>
		
		</stereoisomer>	
		
	<!-- MOLECULE: ISOTOPIC COMPOSITION -->
		<isotope_mixture_type>terrestrial abundance</isotope_mixture_type> <!-- **ABS MANDATORY** Type of isotopic mixture of the molecule, molecular ion or free radical. Enum: {pure isotope, partly substituted, terrestrial abundance} -->
		<isotope_molecules> <!-- **MANDATORY bloc for partly substituted and terrestrial abundances** -->
			<isotope_molecule><!-- multiple -->
				<uid>NULL</uid> <!-- **MANDATORY** LINK to the existing UID of isotopic molecular species contributing to the isotopic molecular mixture [‘MOLEC_(Isotopic)ChemicalFormula(Letter)’, 'MOLION_', 'MOLRAD_' or 'MOLRADION_'] -->
				<mole_fraction>NULL</mole_fraction> <!-- **MANDATORY** Mole fraction of the isotopic molecular species (by default it is the terrestrial one from IUPAC) [Float] -->
			</isotope_molecule>
		</isotope_molecules>
		
	<!-- MOLECULE NUCLEAR SPIN ISOMER COMPOSITION -->
		<nuclear_spin> <!-- **ONLY FOR PURE OR PARTLY SUBSTITUTED ISOTOPE** -->
			<type>equilibrated</type> <!-- **MANDATORY** Nuclear spin isomer of the molecule (or main isotopic species for isotopic mixtures). OpenEnum: {para, ortho, meta, A, E, B_3u, natural, unique, unknown} -->
			<qn_i>NULL</qn_i>  
			<equilibrium_opr>NULL</equilibrium_opr> <!-- **ONLY for equilibrated spin isomers** High temperature equilibrium ortho/para ratio (decimal and fractional). ex: '4.5 (9/2)' -->
			<equilibrium_mpr>NULL</equilibrium_mpr> <!-- **ONLY for equilibrated spin isomers** High temperature equilibrium meta/para ratio (decimal and fractional). ex: '2.5 (5/2)' -->
			<temperature_half_opr></temperature_half_opr> <!-- **ONLY for equilibrated spin isomers** Spin temperature at half the high temperature equilibrium ortho/para ratio [float] (K) -->
			<isomer_molecules> <!-- **MANDATORY bloc for equilibrated (high T) spin isomers** -->
				<isomer_molecule><!-- multiple -->
					<uid>NULL</uid> <!-- **MANDATORY** LINK to the existing UID of a nuclar spin isomer contributing to the equilibrated mixture of spin isomers [‘MOLEC_(Isotopic)ChemicalFormula(Letter)’, 'MOLION_', 'MOLRAD_' or 'MOLRADION_'] -->
					<mole_fraction>NULL</mole_fraction> <!-- **MANDATORY** Equilibrium molar fraction and fractional abundance of the spin isomer at the high temperature limit. Ex: ‘0.3125 (5/16)’ -->
				</isomer_molecule>
			</isomer_molecules>
		</nuclear_spin>  
		
	<!-- MOLECULE SYMMETRIES AND NORMAL VIBRATION MODES	-->
		<symmetry>unknown</symmetry> <!-- **ABS MANDATORY** Symmetry (point group) of the molecule, molecular ion or free radical (or main isotopic species for isotopic mixtures). Enum: {C1, Cs, Ci, Cinfv, Dinfh, C2, C3, C2h, C3h, C2v, C3v, C4v, D2, D3, D2h, D3h, D4h, D5h, D6h, D2d, D3d, D4d, D5d, Td, Oh, Ih, no, unknown} -->
		<case>nonlinear-polyatomic</case> <!-- **ABS MANDATORY** Description of the case category of molecular state for this molecule, molecular ion or radical. Enum: {diatomic, linear-triatomic, nonlinear-triatomic, spherical-top, symmetric-top, asymmetric-top, linear-polyatomic, nonlinear-polyatomic, complex} -->
		<vibrations_number>NULL</vibrations_number> <!-- **MANDATORY** Number ‘n’ of fundamental internal vibration modes ‘Vi’ (normal modes) of the free molecular species [integer] -->
		<vibrations> <!-- **OPTION** Description of the fundamental internal vibrations of the free molecular species -->
			<vibration><!-- multiple --> 
				<mode></mode> <!-- **ABS MANDATORY when this optional bloc is used** Type of fundamental internal vibration of the molecule. Enum: {stretching, stretching sym., stretching antisym., bending, bending in-p, bending out-p, bending sym., bending antisym., bending sym. in-p (scissoring), bending antisym. in-p (rocking), bending sym. out-p (wagging), bending antisym. out-p (twisting), deformation, other, unknown} -->
				<chemical_bond_uid></chemical_bond_uid> <!-- **MANDATORY in option** LINK to the existing UID of the bond (BOND_), part of the molecular species (MOLECPART_) or whole molecule (MOLEC_) subjected to the fundamental vibration [‘BOND_ZAtom1(BondSymbol)ZAtom2(Charge)’ or ‘MOLECPART_’, ‘MOLEC_(Isotopic)ChemicalFormula(Letter)’, 'MOLION_', 'MOLRAD_' or 'MOLRADION_'] -->
				<label></label> <!-- **MANDATORY in option** Label of the normal mode of vibration of the free molecule -->
				<symmetry></symmetry> <!-- Point group symmetry of the normal mode of vibration of the free molecule. Enum: {A, Ap, As, A1, A1p, A1s, A2, A2p, A2s, B, B1, B2, B3, E, Ep, Es, E1, E1p, E1s, E2, E2p, E2s, E3, F, F1, F2, Sigma+, Sigma-, Pi, Phi, Delta} -->
				<degeneracy></degeneracy> <!-- **MANDATORY in option** Degeneracy of the mode of vibration of the free molecule. Enum: {no, double, triple, quadruple} -->
				<fundamental_frequency></fundamental_frequency> <!-- Fundamental frequency of the normal mode of vibration of the free molecule [Float] (cm-1) -->
				<observed_frequency></observed_frequency> <!-- **MANDATORY in option** Observed frequency of the normal mode of vibration of the molecular species [Float] (cm-1) -->
				<observed_frequency_2></observed_frequency_2> <!-- Second observed frequency of the normal mode of vibration of the molecular species [Float] (cm-1) -->
				<harmonic_frequency></harmonic_frequency> <!-- Harmonic frequency of the normal mode of vibration of the free molecule [Float] (cm-1) -->
				<activity_ir></activity_ir> <!-- **MANDATORY in option** IR activity of the normal modes of vibration of the molecular species. Enum: {active, inactive, unknown} -->
				<activity_raman></activity_raman> <!-- **MANDATORY in option** Raman activity of the normal modes of vibration of the molecular species. Enum: {active, inactive, unknown} -->
				<intensity_strength></intensity_strength> <!-- Arbitratry qualitative (relative) intensity of the normal mode of vibration of the free molecule. Enum: {ia, vvw, vw, w, m, s, vs, vvs} -->
				<comments><![CDATA[]]></comments> <!-- Comments on the normal modes of vibration of the molecular species [blob] -->
			</vibration>
		</vibrations>
		<vibrations_comments><![CDATA[]]></vibrations_comments> <!-- Comments on the vibrations of the molecular species [blob] -->
		<vibrations_publications> <!-- **OPTION** -->
			<publication_uid></publication_uid><!-- multiple --> <!-- LINK to the existing UID of the publication in which information on the vibrations of the free molecule has been published [‘PUBLI_FirstAuthorName_Year(Letter)’] -->
		</vibrations_publications>
		
	<!-- MOLECULE PROPERTIES -->	
		<molar_mass>60.008</molar_mass> <!-- **MANDATORY** Molar mass of the isotope or molar weight of the natural mixture, from IUPAC tables [Float] (g/mol) -->
		<state_stp>NULL</state_stp> <!-- **MANDATORY** State of the molecular species in STP conditions (273.15K, 1 bar). Enum: {gas, liquid, solid} -->
		<state_ntp>NULL</state_ntp>
		<protic>aprotic</protic> <!-- **MANDATORY** Ability of the molecular species to form hydrogen bonds (protic) or not. Enum: {protic, aprotic} -->
		<polarity>ionic</polarity> <!-- **MANDATORY** Polarity of the molecular species. Enum: {nonpolar, polar, ionic, amphiphilic} -->
		<dipole></dipole> <!-- Permanent dipole moment of the molecular species [Float] (debye) -->
		
	<!-- MOLECULE REFERENCES + COMMENTS -->
		<links> <!-- **OPTION** Link(s) to web page(s) describing the molecule and its properties -->
			<link><!-- multiple --> 
				<name><![CDATA[Wikipedia: Bromate]]></name> <!-- Name of the web page(s) -->
				<url><![CDATA[https://en.wikipedia.org/wiki/Bromate]]></url> <!-- URL address -->
			</link>
			<link><!-- multiple --> 
				<name><![CDATA[Guidechem: Bromate (8CI,9CI)]]></name> <!-- Name of the web page(s) -->
				<url><![CDATA[https://www.guidechem.com/encyclopedia/bromate-8ci-9ci--dic244574.html]]></url> <!-- URL address -->
			</link>
			<link><!-- multiple --> 
				<name><![CDATA[Pubchem: Bromate]]></name> <!-- Name of the web page(s) -->
				<url><![CDATA[https://pubchem.ncbi.nlm.nih.gov/compound/84979]]></url> <!-- URL address -->
			</link>
		</links>
		<comments><![CDATA[]]></comments> <!-- Additional information on molecular species [blob] -->
	</molecule>
</import>
