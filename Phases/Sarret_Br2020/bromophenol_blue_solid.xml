<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Data type : Fundamental phase

Specific notes :
	
General notes : 
	- Most of the tags are optional, you can remove the really unnecessary ones.
	- Tags marked as 'multiple' can be copied (with its block of sub-tag, up to the ending tag) if needed.
	- all blocs marked "OPTION" can be fully removed if not needed (now or in the future)
	- **ABS MANDATORY / ABS COMPULSORY**: a value need to be absolutely provided, no way to escape! (SSHADE will not function properly if absent).
	- **MANDATORY / COMPULSORY**: very important values for the search of the data. If the value (txt or numeric) of one tag is not known (or irrelevant in your case), then put 'NULL' and write a comment to keep track of the missing value. Remove comment when value is added.
	- **MANDATORY / COMPULSORY only for ...**: when a value is optionally MANDATORY the condition is written. 
	- "LINK to existing UID" (unique identifier): references to another table in SSHADE. You have to reconstruct (easy for some: rule is in comment) or found this existing UID in the database beforehand (use "Provider/Full Search" menu in SSHADE).
	- "UID to CREATE": you need to create this UID using their specific rules of creation that are explained in their attached comment. Use only alphanumeric characters and '_'.
	- Enumeration type ("Enum" or "OpenEnum") must contain one single item from the list given in brackets {}.
	- use a CDATA tag when a value contains at least one special character (ie: &, >, <,...). Example: <![CDATA[AT&T]]> for AT&T
	- The data format is noted beetween [] and is 'ascii' when not specified. Ex: [Float], [Integer], ...  
	- when no numerical format or Enum is specified, it is free text but limited to 256 characters. Only those noted [blob] have no size limitation.
	- to import data for the first time you have to set <import_mode>='first import'. To correct data you have to change it to 'correction'.
	- when a <filename> is given, then the file should be ziped with this xml file for import. 
-->
<import type="fundamental_phase" ssdm_version="0.9.0"
        xmlns="http://sshade.eu/schema/import"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://sshade.eu/schema/import http://sshade.eu/schema/import-0.9.xsd">

<!-- FUNDAMENTAL PHASE: SOLIDS -->
	<solid><!-- multiple -->
		<import_mode>correction</import_mode> <!-- **ABS MANDATORY** Mode of import of the 'solid phase' data. Enum: {first import, ignore, draft, no change, correction} -->
		<uid>SOLID_bromophenol_blue</uid> <!-- **ABS MANDATORY to CREATE** Unique identifier code given to the solid. Should be of the style ‘XXXXXSOLGROUP_GroupName’, ‘XXXXXSOLSERIES_SeriesName’ or ‘XXXXXSOL_SolidName’ where 'XXXXX'is 'MOLEC', 'IONIC', 'COVAL', or 'METAL' depending on <solid_family> -->
		<mineral_uid></mineral_uid> <!-- %%% Chlorargyrite **COMPULSORY when exist** LINK to the existing UID of the equivalent mineral ['MINER_MineralName’] -->
		<liquid_uid></liquid_uid> <!-- **COMPULSORY when exist** LINK to the existing UID of the equivalent liquid ['LIQUID_LiquidFormula’] -->
		
	<!-- NAMES -->
		<official_name>Bromophenol blue</official_name> <!-- **ABS MANDATORY** Official name of the solid, series or (sub)group -->
		<secondary_names> <!-- Alternative names used for the solid -->
			<secondary_name></secondary_name><!-- multiple --> 	
		</secondary_names>
		<cas_number>115-39-9</cas_number> <!-- CAS registry number of the solid ... when available! -->	
		
	<!-- SOLID CLASS AND COMPOUND TYPE -->
		<family>molecular solid</family> <!-- **ABS MANDATORY** Family (or major class) of the solid. Enum: {molecular solid, covalent network solid, ionic solid, metallic solid} -->
		<class>polar molecular solid</class> <!-- %%% TBV **ABS MANDATORY** Class of solid. Enum: {non polar molecular solid, polar molecular solid, hydrogen bonded molecular solid, mixed molecular solid, chain covalent network solid, sheet covalent network solid, tridimentional covalent network solid, glass, acid salt, alkali salt, normal salt, mixed salt, true metal, pseudometal (semi-conductor)} -->
		<compound_type>organic molecular solid</compound_type> <!-- **MANDATORY** Type of solid compound. OpenEnum: {elemental solid, noble gas solid, carbon allotrope, organic molecular solid, inorganic molecular solid, clathrate, clathrate hydrate, hydrate, homopolymer, copolymer, molecular solid solution, solid molecular mixture, oxide-hydroxide, non-oxide ceramic, acetate, arsenate, borate, carbonate, chloride, chlorite, chlorate, perchlorate, chromate, cyanide, cyanate, fluoride, fulminate, molybdate, nitrite, nitrate, phosphite, phosphate, sulfide, sulfite, sulfate, tungstate, vanadate, organic salt, anhydrous salt, hydrated salt, silicate, nesosilicate, sorosilicate, cyclosilicate, inosilicate, phyllosilicate, tectosilicate, metal, metallic alloy, semi-conductor, other compound, …} -->	
		<classification_level>unique solid</classification_level> <!-- **ABS MANDATORY** Level of solid classification. Enum: {solid group, solid subgroup, solid polymorphs, solid solution series, variable solid, unique solid} -->

	<!-- (END)MEMBERS, POLYMORPHS AND POLYTYPES -->
		<endmembers> <!-- **MANDATORY for solid group & subgroup, solid solution series** List of the endmember solids of the series or group -->
			<solid_uid></solid_uid><!-- multiple --> <!-- **MANDATORY** LINK to the existing UID of the endmember solid ['MOLECSOL_', 'IONICSOL_', 'COVALSOL_' or 'METALSOL_SolidName’] -->
		</endmembers>	
		<polymorphs> <!-- **MANDATORY for solid polymorphs** List of the solids polymorphs -->
			<polymorph><!-- multiple -->
				<solid_uid></solid_uid><!-- multiple --> <!-- **MANDATORY** LINK to the existing UID of the solid polymorph ['MOLECSOL_', 'IONICSOL_', 'COVALSOL_' or 'METALSOL_SolidName’] -->
				<comments><![CDATA[]]></comments> <!-- Additional information on solid polymorph (P-T stability domain ...) [blob] -->
			</polymorph>
		</polymorphs>		
		<polytypes> <!-- **ONLY for polymorphs, solution series and solids if exist** List of the solid polytypes -->
			<polytype> <!-- multiple --> 
				<name></name> <!-- Name of the solid polytype -->
				<comments><![CDATA[]]></comments> <!-- Additional information on the solid polytype [blob] -->
			</polytype>
		</polytypes> 
		<figures><!-- Figures on the solid (OPTION) -->
			<figure><!-- multiple -->
				<filename></filename> <!-- File name (with .png, .jpg, .gif extension) of the figure on the solid (phase diagram or solution series diagram...). The file should be ziped with this xml file for import -->
				<caption><![CDATA[]]></caption> <!-- Caption or comment on the figure -->
			</figure>
		</figures>
	
	<!-- CHEMICAL COMPOSITION -->
		<formula><![CDATA[$((C_6H_2)Br_2OH)_2C(C_6H_4)SO_3$]]></formula> <!-- **ABS MANDATORY** Developed structural (empirical) chemical formula of the solid (Latex format) -->
		<chemical_formula>(C6H2Br2OH)2C(C6H4)SO3</chemical_formula> <!-- **ABS MANDATORY** Developed global chemical formula of the solid -->
		<hydration>no</hydration> <!-- **ABS MANDATORY** Tell if the solid contains structural H2O. BoolEnum: {yes, no} or {true, false} -->
		<hydration_series></hydration_series> <!-- **ABS MANDATORY when "hydration"='true'** Flag telling if the solid is an isostructural series of n-hydrated solids (true). BoolEnum: {yes, no} or {true, false} -->
		<hydration_number></hydration_number> <!-- **MANDATORY for hydration** Number or range of numbers of structural H2O in the solid -->
		
	<!-- BONDS -->	
		<chemical_functions> <!-- List of the different chemical functions present in the solid, except those already in the molecular "species" -->
			<chemical_function><!-- multiple -->
				<uid>NULL</uid> <!-- **MANDATORY** LINK to the existing UID of the chemical functions of the solid  [‘GROUPMOLEC_RGroupformula’, ‘GROUPION_ RGroupformulaCharge’, ‘GROUPRAD_ RGroupformula’, ‘MINION_IonformulaCharge’] -->
				<number>NULL</number> <!-- Number or range of numbers of this chemical function present in the solid -->
			</chemical_function>
		</chemical_functions>
		<chemical_bonds> 
			<chemical_bond>
				<uid></uid>
				<number></number> 
			</chemical_bond>
		</chemical_bonds>
		
	<!-- MOLECULAR OR ATOMIC COMPOSITION -->
		<isotope_mixture_type>terrestrial abundance</isotope_mixture_type> <!-- **ABS MANDATORY** Type of isotopic mixture of the solid. Enum: {pure isotope, partly substituted, terrestrial abundance} -->
		<species> <!-- **ABS MANDATORY** list of the different molecular or atomic species of the solid and their number -->
			<specie><!-- multiple -->
				<uid>MOLEC_bromophenol_blue</uid> <!-- **ABS MANDATORY* LINK to the existing UID of the molecular species (molecule or molecular ion) or element (atom or ion) composing the solid. [‘MOLEC_(Isotopic)Chemicalformula(Letter)’, ‘MOLION_(Isotopic)ChemicalformulaCharge(Letter)’, 'MOLRAD_' or 'MOLRADION_'] or [‘MINION_AOn_Charge’] or [‘ATOM_(Z)Symbol’ or 'ATION_(Z)SymbolCharge'] -->
				<number_min>1</number_min> <!-- **MANDATORY** Total number of molecular/atomic species with fixed abundance OR minimum number for molecular/atomic species with variable abundance in solid solutions, n-hydrated solids (H2O), or in solid (sub-)group [Float] -->
				<state>pure</state> <!-- **MANDATORY** State of the molecular/atomic species inside the solid. OpenEnum: {constituent element, constituent cation, constituent anion, anionic radical, pure, mixed, matrix, monomers, dimers, multimers, solute, solvent, solid solution, clathrate network, clathrate guest, hydration, hydrated, interlayer physically adsorbed, other, unknown, …} -->
				<relevance>main</relevance> <!-- **ABS MANDATORY** Relevance of each molecular/atomic species in the solid. Enum: {main, substituted} -->
			</specie>
		</species>
		<composition_comments><![CDATA[]]></composition_comments><!-- Any additional information (range of values of x,n, sum(x+y+z), …) on the composition and formula of the solid [blob] -->
			
	<!-- CLASSIFICATION -->
		<!-- see SSDM or 'Solid Classification' document for details -->
		<classification_class>02</classification_class> <!-- %%% TBV **ABS MANDATORY** Class of the solid in the classification. OpenEnum: {01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, various} -->
		<classification_type>02.01</classification_type> <!-- %%% TBV **(ABS) MANDATORY** Type of the solid in the classification. OpenEnum: {01.01, 01.02, 01.03, 01.04, …, 04.01, 04.02, …, 14.04} -->
		<classification_group>02.01.05</classification_group> <!-- **(ABS) MANDATORY** Group of the solid in the classification. OpenEnum: {..., 04.01.01, 04.01.02, 04.01.03, ..., N/A [TBD]} -->
		<classification_code>NULL</classification_code> <!-- **(ABS) MANDATORY only for unique and variable solid** Code of the solid in the classification (Ex: 04.02.03.07 [TBD])-->
			
	<!-- CRYSTALLOGRAPHY -->
		<phase_name>NULL</phase_name> <!-- **MANDATORY** Common name of the solid phase as used in the literature -->
		<phase_type>crystalline</phase_type> <!-- **ABS MANDATORY** Main type of phase of the solid. Enum: {crystalline, semicrystalline, disordered, quasi-amorphous, amorphous, glassy, mesophase, mixed, various, unknown} -->
		<crystal_system>NULL</crystal_system> <!-- **MANDATORY** Crystal lattice system of the solid. Enum: {triclinic, monoclinic, orthorhombic, trigonal, hexagonal, tetragonal, isometric (cubic), amorphous, N/A, various, unknown} -->
		<crystal_class>NULL</crystal_class> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Crystalline class name of the solid. Enum: {pedial, pinacoidal, domatic, sphenoidal, prismatic, pyramidal, disphenoidal, dipyramidal, rhombohedral, ditrigonal-pyramidal, trapezohedral, hexagonal-scalenohedral, trigonal-dipyramidal, ditrigonal-dipyramidal, dihexagonal-pyramidal, dihexagonal-dipyramidal, scalenohedral, ditetragonal-pyramidal, ditetragonal-dipyramidal, tetartoidal, diploidal, hexatetrahedral, gyroidal, hexoctahedral, N/A, various, unknown} -->
		<crystal_class_symbol>NULL</crystal_class_symbol> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Crystalline class (point group) Hermann-Mauguin short symbol (and Schönflies symbol) of the solid. Enum: {1 (C1), -1 (Ci), 2 (C2), m (Cs), 2/m (C2h), 222 (D2), mm2 (C2v), mmm (D2h), 3 (C3), -3 (S6), 32 (D3), 3m (C3v), -3m (D3d), 4 (C4), -4 (S4), 4/m (C4h), 422 (D4), 4mm (C4v), -42m (D2d), 4/mmm (D4h), 6 (C6), -6 (C3h), 6/m (C6h), 622 (D6), 6mm (C6v), -6m2 (D3h), 6/mmm (D6h), 23 (T), m-3 (Th), 432 (O), -43m (Td), m-3m (Oh), N/A, various, unknown} -->  
		<crystal_spacegroup>NULL</crystal_spacegroup> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Crystalline Hermann-Mauguin symbol of symmetry space group in point group of the solid. Enum: {Aba2, Abm2, Ama2, Amm2, C2, C2/c, C2/m, C222, C222(1), Cc, Ccc2, Ccca, Cccm, Cm, Cmc2(1), Cmca, Cmcm, Cmm2, Cmma, Cmmm, F222, F23, F432, F-43c, F-43m, F4(1)32, Fd-3, Fd-3c, Fd-3m, Fdd2, Fddd, Fm-3, Fm-3c, Fm-3m, Fmm2, Fmmm, I222, I23, I2(1)2(1)2(1), I2(1)3, I4, I4/m, I4/mcm, I4/mmm, I41cd, I41md, I422, I432, I4cm, I4mm, I-4, I-42d, I-42m, I-43d, I-43m, I-4c2, I-4m2, I4(1), I4(1)/a, I4(1)/acd, I4(1)/amd, I4(1)22, I4(1)32, Ia-3, Ia-3d, Iba2, Ibam, Ibca, Im-3, Im-3m, Ima2, Imm2, Imma, Immm, P1, P-1, P2, P2/c, P2/m, P222, P222(1), P23, P2(1), P2(1)/c, P2(1)/m, P2(1)2(1)2, P2(1)2(1)2(1), P2(1)3, P3, P312, P31c, P31m, P321, P3c1, P3m1, P-3, P-31c, P-31m, P-3c1, P-3m1, P3(1), P3(1)12, P3(1)21, P3(2), P3(2)12, P3(2)21, P4, P4/m, P4/mbm, P4/mcc, P4/mmm, P4/mnc, P4/n, P4/nbm, P4/ncc, P4/nmm, P4/nnc, P422, P42bc, P42cm, P42mc, P42nm, P42(1)2, P432, P-4, P-42c, P-42m, P-42(1)c, P-42(1)m, P-43m, P-43n, P-4b2, P-4c2, P-4m2, P-4n2, P4(1), P4(1)22, P4(1)2(1)2, P4(1)32, P4(2), P4(2)/m, P4(2)/mbc, P4(2)/mcm, P4(2)/mmc, P4(2)/mnm, P4(2)/n, P4(2)/nbc, P4(2)/ncm, P4(2)/nmc, P4(2)/nnm, P4(2)22, P4(2)2(1)2, P4(2)32, P4(3), P4(3)22, P4(3)2(1)2, P4(3)32, P4bm, P4cc, P4mm, P4nc, P6, P6/m, P6/mcc, P6/mmm, P622, P6cc, P6mm, P-6, P-62c, P-62m, P-6c2, P-6m2, P6(1), P6(1)22, P6(2), P6(2)22, P6(3), P6(3)/m, P6(3)/mcm, P6(3)/mmc, P6(3)22, P6(3)cm, P6(3)mc, P6(4), P6(4)22, P6(5), P6(5)22, Pa-3, Pba2, Pbam, Pban, Pbca, Pbcm, Pbcn, Pc, Pca2(1), Pcc2, Pcca, Pccm, Pccn, Pm, Pm-3, Pm-3m, Pm-3n, Pma2, Pmc2(1), Pmm2, Pmma, Pmmm, Pmn2(1), Pmmn, Pmna, Pn-3, Pn-3m, Pn-3n, Pna2(1), Pnc2, Pnma, Pnn2, Pnna, Pnnm, Pnnn, R3, R32, R3c, R3m, R-3, R-3c, R-3m, N/A, various, unknown} -->
		
	<!-- CRYSTAL SITES -->
		<crystal_sites> <!-- **OPTION** Crystallographic sites of the solid structure -->
			<crystal_site><!-- multiple -->
				<label></label> <!-- **ABS MANDATORY when this optional bloc is used** Label of the crystallographic site of the molecular/atomic species in the solid structure. FreeList: {M1, M2, M3, M4, O1, O2, O3, O4, O5, O6, O7, O8,…, 5-12, 5-12_6-4, 5-12_6-4, 4-3_5-6_6-3, 5-12_6-8, …} -->
				<type></type> <!-- **MANDATORY for OPTION** Type of the crystallographic site of the molecular/atomic species in the solid structure. Enum: {main, substitutional, interstitial, clathrate cage, unknown [TBC]} -->
				<number></number> <!-- **MANDATORY for OPTION** Numbers of equivalent crystallographic sites in the elementary cell of the solid structure [integer] -->
				<wp></wp> <!-- Wyckoff position code of the crystallographic site of the solid structure (multiplicity, Wyckoff letter, site symmetry). Ex: ‘4u1’, ‘2m.2.’, ‘1e222’ -->
				<species> <!-- **MANDATORY for OPTION** --><!-- List and state of the molecular/atomic species in the crystallographic site -->
					<specie><!-- multiple -->
						<uid></uid> <!-- **MANDATORY** LINK to the existing UID of the natural molecular/atomic species [‘MOLEC_(Isotopic)Chemicalformula(Letter)’, ‘MOLION_(Isotopic)ChemicalformulaCharge(Letter)’, 'MOLRAD_' or 'MOLRADION_'] or [‘ATOM_(Z)Symbol’ or 'ATION_(Z)SymbolCharge'] -->
						<coordinence></coordinence> <!-- Coordinence of the molecule/atom and associated geometry of the crystallographic site. Enum: {linear I, linear II, trigonal planar III, tetrahedral IV, square planar IV, trigonal bipyramidal V, square pyramidal V, octahedral VI, trigonal prismatic VI, pentagonal bipyramidal VII, face capped octahedral VII, trigonal prismatic square face monocapped VII, cubic VIII, square antiprismatic VIII, dodecahedral VIII, hexagonal bipyramidal VIII, octahedral trans bicapped VIII, trigonal prismatic triangular face bicapped VIII, trigonal prismatic square face bicapped VIII, tricapped trigonal prismatic IX, monocapped square antiprismatic IX, bicapped square antiprismatic X, trigonal prismatic all faces capped XI, icosahedral XII, cuboctahedral XII, anticuboctahedral XII, hexagonal prismatic XII, bicapped hexagonal antiprismatic XIV, ..., pentagonal dodecahedron XX, hexagonal truncated trapezohedron XXIV, hexadecahedron XXVIII, no, unknown [TBC]} -->
						<occupancy></occupancy> <!-- Fractional occupancy of the crystallographic site [Float] -->
						 
						<oxidation_state></oxidation_state> <!-- Oxidation state (or number) of the atom (mostly cation) in the crystallographic site [integer] -->
					</specie>
				</species>
			</crystal_site>
		</crystal_sites>
		<crystal_composition_order></crystal_composition_order> <!-- Type of compositional order in the crystallographic sites of the solid structure. Enum: {ordered, disordered, partly ordered, unknown} -->
		<crystal_comments><![CDATA[]]></crystal_comments> <!-- Additional information on solid crystal structure and sites [blob] -->
		
	<!-- PROPERTIES -->
		<molar_mass>669.96</molar_mass> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Value or range of molar mass of the solid (g/mol) -->
		<density>2.2 (predicted)</density> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Value or range of density of the solid (g/cm3) -->
		<state_ntp>solid</state_ntp> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** State of the solid in NTP conditions (293.15K, 1 atm). Enum: {solid, metastable solid - other solid, metastable solid - liquid, other solid, liquid, gas} -->
		<phase_transitions> <!-- **MANDATORY only for unique and variable solids** -->
			<phase_transition><!-- multiple -->
				<type>solid-liquid</type> <!-- **MANDATORY** Type of phase transition of the solid. Enum: {solid-solid, solid-liquid, triple point solid-solid-solid, triple point solid-solid-liquid, triple point solid-liquid-gas, quadruple point} -->
				<other_phases>liquid</other_phases> <!-- **MANDATORY** Name(s) of the other phase(s) implied in the phase transition of the solid. Ex: 'amorphous Water ice Ia' for Water ice Ic, 'water ice Ih, liquid, gas' for triple point of water -->
				<temperature><![CDATA[273°C (546K)]]></temperature> <!-- **MANDATORY** Temperature (value(s) or range) of the phase transition of the solid at defined pressure, or average value when variable (unit T: K; unit P: multiples of ‘Pa’ or ‘bar’) Ex: ’35.4 K (100 mbar)’, ‘272.3 K (1 bar) – 275.2 K (100 bar)’-->
			</phase_transition>
		</phase_transitions>
		
	<!-- OPTICAL PROPERTIES -->
		<refringence_type>unknown</refringence_type> <!-- **MANDATORY only for unique and variable solids, and solid solutions** Type of refringence of the solid crystal. Enum: {isotropic, uniaxial, biaxial, various, unknown} -->
		<refringence_sign></refringence_sign> <!-- **MANDATORY only for unique solid** Sign of birefringence of the solid crystal. Enum: {positif, negatif, positif or negatif, no, unknown} -->
		<birefringence></birefringence> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Value or range of birefringence, Delta_n, of the solid crystal -->
		<refraction_index_na>1.743</refraction_index_na> <!-- **MANDATORY only for unique solid, variable solid, and solid solution series** Value or range of refraction index n_alpha of the solid crystal -->
		<refraction_index_nb></refraction_index_nb> <!-- **MANDATORY only for unique, variable and solution uniaxial and biaxial solids** Value or range of refraction index n_beta of the solid crystal-->
		<refraction_index_ng></refraction_index_ng> <!-- **MANDATORY only for unique, variable and solution biaxial solid** Value or range of refraction index n_gamma of the solid crystal -->
		<dispersion_v></dispersion_v> <!-- **Only for transparent crystals** Value, range or qualitative information of the Abbe number V expressing the visible dispersion of a transparent solid crystal -->
		<pure_color>pink</pure_color> <!-- Main color of the pure solid crystals (without any impurity). FreeList: {colorless, white, blue, bluish, azure, green, greenish, emerald, olive, turquoise, yellow, yellowish, orange, pink, pinkish, red, reddish, lilac, violet, indigo, purple, brown, brownish, beige, gray, grayish, black, blackish, bronze, lead, silver, steel, copper, tin, various,…} -->
		<true_color>light pink to purple (Yellow to blue in solution, depending on pH)</true_color> <!-- Color nuance (pure) and other known colors of the solid crystals (with impurity, ...) -->
		<diaphaneity></diaphaneity> <!-- Capacity of the solid to transmit light. Enum: {transparent, transparent to translucent, transparent to subtranslucent, transparent to opaque, translucent, translucent to subtranslucent, translucent to opaque, subtranslucent to opaque, opaque, various, unknown} -->
		<luster></luster> <!-- Description of how and how much the surface of a solid reflects light. Enum: {metallic, submetallic, waxy, vitreous, pearly, silky, greasy, resinous, adamantine, earthy, various, unknown} -->
		
	<!-- REFERENCES AND COMMENTS -->
		<publications> <!-- **OPTION** List of the publications on the fundamental solid phase and its properties -->
			<publication_uid></publication_uid><!-- multiple --> <!-- LINK to the existing UID of the publication [‘PUBLI_FirstAuthorName_Year(Letter)’] -->
		</publications>
		<links> <!-- **OPTION** --><!-- Link(s) to web page(s) describing the solid and its properties -->
			<link>
				<name><![CDATA[Wikipedia: Bromophenol blue]]></name>
				<url><![CDATA[https://en.wikipedia.org/wiki/Bromophenol_blue]]></url>
			</link>
			<link>
				<name><![CDATA[Guidechem: Phenol,4,4'-(1,1-dioxido-3H-2,1-benzoxathiol-3-ylidene)bis[2,6-dibromo-]]></name>
				<url><![CDATA[https://www.guidechem.com/encyclopedia/phenol-4-4-1-1-dioxido-3h-2-1--dic2415.html]]></url>
			</link>
			<link>
				<name><![CDATA[PubChem: Bromophenol blue]]></name>
				<url><![CDATA[https://pubchem.ncbi.nlm.nih.gov/compound/8272]]></url>
			</link>
		</links>
		<comments><![CDATA[]]></comments> <!-- Additional information on solid (possible replacement elements, impurities, …) [blob] -->
	</solid>
</import>
